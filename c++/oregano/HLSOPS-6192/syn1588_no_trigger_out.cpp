// syn1588_example_trigger_period
//
// This simple examples shows how to generate periodic or one-time events
// using the syn1588 hardware. A 10 Hz output signal is generated and routed
// to an output pin.
// One-time events are generated using the TRIGGER resources, periodic events
// are generated using the PERIOD resources. Both types may be generated using
// the generateEvent API function.

// This example is extracted from the source code of the CSS. It is effectively
// using the same code for configuring the syn1588PCIe NIC that the CSS does.
// Classes:
//   Oregano_syn1588: The primary class for configuring and communicating with
//                    the syn1588PCIe NIC
//   InteruptBits: A union class used to interpret the IREN and IRSRC register
//                 for reads/writes.
//   PeriodTime: A union class used to interpret the PERIODTIME0H/L registers
//               for reads/writes.
//   IOMatrix: A typedef union used to interpret the IOMATRIX register for
//              reads/writes.
//   EventCtrl: A typedef union used to interpret the EVENTCTRL register for
//              reads/writes.
//
//   main()
//   A rather simple main that:
//    - Instantiates an instance of Oregano_syn1588
//        * Configures SMA1 (X7) to output Period0
//        * Sets up the IOMATRIX to connect PERIOD0 to Event0
//    - Starts a thread that is used to monitor for Event0 interrupts and log
//      their timestamps
//    - Configures the PERIOD0 registers for the defined PERIOD_TIME and enables
//      a square wave output on X7
//    - Monitors for N Event0 occurrences
//    - Tears down.
//
//  Modify PERIOD_TIME to change the trigger period. Value is in seconds.
//  Modify NUM_EVENTS_TO_WAIT_FOR to set how many events should occur.

#include "syn1588_no_trigger_out.h"

#include "ptp/log.hpp"

#include <atomic>
#include <chrono>
#include <iomanip>
#include <iostream>
#include <stdio.h>
#include <thread>
#include <unistd.h>

std::atomic_bool terminateEventMonitor;
std::atomic_bool eventMonitorRunning(false);
std::atomic_bool event0occurred;
using test::Oregano_Syn1588;
using test::EEvent;


#define NUM_EVENTS_TO_WAIT_FOR 6   // Number of trigger events to wait for
#define PERIOD_TIME 5.0            // Trigger period (in seconds)

namespace test {

using ptp::Logger;
using test::EEvent;

#define CARD_NUM 0           // card number 0 is the first found syn1588 card.

#define EXPOSURE_TIME 0.020  // in seconds
#define LOG_LVL ptp::log::Level::debug


//==============================================================================
// CLASS: InterruptBits
//

//------------------------ operator=(InterruptBits) ----------------------------
//
test::InterruptBits& InterruptBits::operator=( const test::InterruptBits& rhs)
{
  if( this == &rhs )            // check for assignment to self
    return *this;

  this->word = rhs.word;

  return *this;
}
// End of operator=(InterruptBits)


//==============================================================================
// CLASS: PeriodTime
//

//-------------------------- operator=(PeriodTime) -----------------------------
//
PeriodTime& PeriodTime::operator=( const PeriodTime& rhs)
{
  if( this == &rhs )            // check for assignment to self
    return *this;

  this->reg.low  = rhs.reg.low;
  this->reg.high = rhs.reg.high;

  return *this;
}
// End of operator=(PeriodTime)


//------------------------- operator<<(PeriodTime) -----------------------------
//
// Inserts a period time struct/union onto an output stream

std::ostream& operator<<(std::ostream& os, const PeriodTime& rhs)
{
  std::ios_base::fmtflags prevFlags;
  std::ostringstream tmpStr;

  // Save current stream flags while setting desired ones
  prevFlags = os.flags( std::ios::hex );

  os << std::hex << std::setfill('0');
  os << "PeriodTime: "
      << "H=0x" << std::hex << std::setw(8) << rhs.reg.high << ", "
      << "L=0x" << std::hex << std::setw(8) << rhs.reg.low << ", "
      << std::dec << rhs.field.seconds << "."
      << rhs.field.nanoseconds << "," << rhs.field.subNanoseconds << "sec";

  // Restore previous stream flags
  os << std::dec;
  os.flags( prevFlags );

  return os;
}
// End of operator<<(PeriodTime)


//==============================================================================
// CLASS: TrigTime
//

TrigTime& TrigTime::operator=( const TrigTime& rhs)
{
  if( this == &rhs )            // check for assignment to self
    return *this;

  this->reg.low  = rhs.reg.low;
  this->reg.high = rhs.reg.high;

  return *this;
}
// End of operator=(TrigTime)


//------------------------------- operator<<() ---------------------------------
//
// Inserts a trigger time struct/union onto an output stream

std::ostream& operator<<(std::ostream& os, const TrigTime& rhs)
{
  std::ios_base::fmtflags prevFlags;
  std::ostringstream tmpStr;

  // Save current stream flags while setting desired ones
  prevFlags = os.flags( std::ios::hex );

  os << std::hex << std::setfill('0');
  os << "TriggerTime: "
      << "H=0x" << std::hex << std::setw(8) << rhs.reg.high << ", "
      << "L=0x" << std::hex << std::setw(8) << rhs.reg.low << ", "
      << std::dec << rhs.field.seconds << "."
      << rhs.field.nanoseconds << "sec, ";
  if( rhs.field.stateChange == 0 || rhs.field.stateChange == 3 )
    os << "StateChange on Trigger out=Set output to 0, ";
  else if( rhs.field.stateChange == 1 )
    os << "StateChange on Trigger out=Set output to 1, ";
  else
    os << "StateChange on Trigger out=Toggle output, ";

  // Restore previous stream flags
  os << std::dec;
  os.flags( prevFlags );

  return os;
}
// End of operator<<(TrigTime)


//==============================================================================
// CLASS: Oregano_Syn1588
//

//------------------------------------------------------------------------------
// Oregano_Syn1588: PRIVATE
//------------------------------------------------------------------------------

std::string Oregano_Syn1588::iomDecodeSource( const std::uint8_t src )
{
  std::string source;

  switch( src ) {
    case Syn1588Ifc::IOM_SRC_DIS:
      source = " <-- Disabled (output tri-state)";
      break;
    case Syn1588Ifc::IOM_SRC_SMA0:
      source = " <-- SMA0 (X4)";
      break;
    case Syn1588Ifc::IOM_SRC_SMA3:
      source = " <-- SMA3 (X5)";
      break;
    case Syn1588Ifc::IOM_SRC_SMA2:
      source = " <-- SMA2 (X6)";
      break;
    case Syn1588Ifc::IOM_SRC_SMA1:
      source = " <-- SMA1 (X7)";
      break;
    case Syn1588Ifc::IOM_SRC_PER0:
      source = " <-- period0_o of Clock_M";
      break;
    case Syn1588Ifc::IOM_SRC_PER1:
      source = " <-- period1_o of Clock_M";
      break;
    case Syn1588Ifc::IOM_SRC_TRG0:
      source = " <-- trigger0_o of Clock_M";
      break;
    case Syn1588Ifc::IOM_SRC_TRG1:
      source = " <-- trigger1_o of Clock_M";
      break;
    case Syn1588Ifc::IOM_SRC_1PPS:
      source = " <-- 1pps_o of Clock_M";
      break;
    case Syn1588Ifc::IOM_SRC_IRIGB:
      source = " <-- IRIG-B signal of Clock_M";
      break;
    case Syn1588Ifc::IOM_SRC_IRIGB + 1:
    case Syn1588Ifc::IOM_SRC_IRIGB + 2:
      source = " <-- RESERVED";
      break;
    default:
      source = " <-- Disabled (output tri-state)";
      break;
  }

  return source;
}
// End of iomDecodeSource()


//------------------------------ iomToString() ---------------------------------
//

std::string Oregano_Syn1588::iomToString( const IOMatrix& iom )
{
  std::ostringstream iomStr;

  iomStr << std::hex << std::setfill('0');
  iomStr << "syn1588 IOMATRIX("
         << "0x" << std::setw(4) << Syn1588Ifc::IOMATRIX << ")="
         << "0x" << std::setw(8) << iom.word << ":" << std::endl;

  iomStr << "     SINK <--IOMATRIX--> SOURCE" << std::endl
         << "---------------     ---------------" << std::endl
         << "(3:0)  SMA0(X4)" << iomDecodeSource( iom.field._00_4_sma0X4 ) << std::endl
         << "(7:4)  SMA1(X7)" << iomDecodeSource( iom.field._04_4_sma1X7 ) << std::endl
         << "(11:8) SMA2(X6)" << iomDecodeSource( iom.field._08_4_sma2X6 ) << std::endl
         << "(15:12)SMA3(X5)" << iomDecodeSource( iom.field._12_4_sma3X5 ) << std::endl
         << "(19:16)  EVENT0" << iomDecodeSource( iom.field._16_4_event0 ) << std::endl
         << "(23:20)  EVENT1" << iomDecodeSource( iom.field._20_4_event1 ) << std::endl
         << "(24)   LVL0(X4)  :  Active "
         << ( (iom.field._24_1_level0 == 1) ? "Low" : "High") << std::endl
         << "(25)   LVL1(X7)  :  Active "
         << ( (iom.field._25_1_level1 == 1) ? "Low" : "High") << std::endl
         << "(26)   LVL2(X6)  :  Active "
         << ( (iom.field._26_1_level2 == 1) ? "Low" : "High") << std::endl
         << "(27)   LVL3(X5)  :  Active "
         << ( (iom.field._27_1_level3 == 1) ? "Low" : "High") << std::endl
         << "(31:28)RESERVED" << std::endl;

  return iomStr.str();
}
// End of iomToString()


// Utility method to wait for some number of milliseconds
void delay(double time_s )
{
  int dt;
  int delay_ms = (time_s * 1000.0);
  auto start = std::chrono::steady_clock::now();
  do {
    auto curr = std::chrono::steady_clock::now();
    dt = std::chrono::duration<double, std::milli>(curr-start).count();
  } while( dt < delay_ms );
}
// End of delay()


//-------------------------- clearEventInterrupt() -----------------------------
//
void Oregano_Syn1588::clearEventInterrupt( const EEvent& who )
{
  InterruptBits irsrc;

  irsrc.field._10_1_event0 = (who.isEvent0()) ? 1 : 0;
  irsrc.field._11_1_event1 = (who.isEvent1()) ? 1 : 0;

  syn1588Ifc->writeReg( Syn1588Ifc::IRSRC, ~irsrc.word );
}
// End of Oregano_Syn1588::clearEventInterrupt()


//------------------------- disableEventInterrupt() ----------------------------
//
void Oregano_Syn1588::disableEventInterrupt( const EEvent& who )
{
  std::ostringstream msgStr;

  if( who.isNoEvent() )
    return;

  InterruptBits iren( syn1588Ifc->readReg( Syn1588Ifc::IREN ) );

  msgStr << "disableEventInterrupt(): Entry=0x"
    << std::hex << std::setfill('0') << std::setw(8) << iren.word;

  if( who.isAllEvents() )
    iren.field._10_1_event0 = iren.field._11_1_event1 = 0;
  else if( who.isEvent0() )
    iren.field._10_1_event0 = 0;
  else
    iren.field._11_1_event1 = 0;

  msgStr << ", Write=0x"
    << std::hex << std::setfill('0') << std::setw(8) << iren.word;

  syn1588Ifc->writeReg( Syn1588Ifc::IREN, iren.word );

  iren = syn1588Ifc->readReg( Syn1588Ifc::IREN );
  msgStr << ", Exit=0x"
    << std::hex << std::setfill('0') << std::setw(8) << iren.word;
  std::cout << msgStr.str() << std::endl;
}
// End of Oregano_Syn1588::disableEventInterrupt()


//-------------------------- disablePeriodicOut() ------------------------------
//

void Oregano_Syn1588::disablePeriodicOut()
{
  EventCtrl eventctrl;
  std::ostringstream eventctrlStr;

  // Note: Disabling of periodic output is asynchronous and could occur when
  // the output is either asserted or de-asserted. If asserted then we need to
  // return it to the de-asserted state.
  // EVENTCTRL contains the proper init/set values for PERIOD0 but they only
  // take effect if the period output is disabled or is being enabled. Since
  // we just disabled the period output on the first write above, write the
  // EVENTCTRL value again to effect the PERIOD0 set/init values thus returning
  // the output to the disabled state.
  // To assure the ability of being able to enable a new PERIOD0 at a new
  // TRIGTIME0, it is necessary to incorporate a software workaround provided
  // by Oregano. This workaround disables PERIOD0, sets a new very long period
  // time, re-enables period0 with this new very long period, and then disables
  // again.

  eventctrl.word = syn1588Ifc->readReg( Syn1588Ifc::EVENTCTRL );

  eventctrlStr << "Entry=0x"
      << std::hex << std::setfill('0') << std::setw(8) << eventctrl.word;

  // Disable TRIGGER0, PERIOD0, PERIOD0START
  eventctrl.field._04_1_period0 =          // disable period0
  eventctrl.field._06_1_period0out = 0;    // disable period0out
  syn1588Ifc->writeReg( Syn1588Ifc::EVENTCTRL, eventctrl.word );

  eventctrlStr << "  Write #1=0x"
      << std::hex << std::setfill('0') << std::setw(8) << eventctrl.word;

  // Now that PERIOD0 is disabled, assure output won't change when the
  // work-around (i.e. long period) is enabled by setting both the
  // period0init and perid0set value to the current sense.
  // Also, make sure that the trigger0 is disabled before writing.
  eventctrl.field._02_1_trigger0 = 0;

  // make sure initial and set value of period0 output match the
  // set (de-asserted) value of the period sense.
  eventctrl.field._08_1_period0init =
  eventctrl.field._09_1_period0set  = period0Sense.field._09_1_period0set;
  syn1588Ifc->writeReg( Syn1588Ifc::EVENTCTRL, eventctrl.word );

  eventctrlStr << "  Write #2=0x"
      << std::hex << std::setfill('0') << std::setw(8) << eventctrl.word;

  // Next, start a very long period on PERIOD0 immediately (without trigger)
  // Set periodtime and then enable period0 to start immediately...
  uint32_t periodtimeH = 0xEFFFFFFF;        // very long period
  uint32_t periodtimeL = 0x0;
  syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::PERIODTIME0_L, periodtimeL );
  syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::PERIODTIME0_H, periodtimeH );

  // Enable period0
  eventctrl.word = syn1588->getSyn1588Ifc()->readReg( Syn1588Ifc::EVENTCTRL );
  eventctrl.field._04_1_period0 = 1;        // enable period0
  eventctrl.field._06_1_period0out =        // disable period0out
  eventctrl.field._15_2_period0strt = 0;    // start period0 now (no trigger:
                                            // remove bits in case they were set
                                            // before)
  syn1588Ifc->writeReg( Syn1588Ifc::EVENTCTRL, eventctrl.word );

  eventctrlStr << "  Write #3=0x"
      << std::hex << std::setfill('0') << std::setw(8) << eventctrl.word;

  // Now stop the long period work-around period.
  eventctrl.word = syn1588Ifc->readReg( Syn1588Ifc::EVENTCTRL );
  eventctrl.field._04_1_period0 =          // diasble period0
  eventctrl.field._06_1_period0out = 0;    // disable period0out
  syn1588Ifc->writeReg( Syn1588Ifc::EVENTCTRL, eventctrl.word );
  // END of SW workaround.
  // The next period and trigtime0 can be programmed without penalty of delay.

  eventctrlStr << "  Exit=0x" << std::hex << std::setfill('0') << std::setw(8)
      << syn1588Ifc->readReg( Syn1588Ifc::EVENTCTRL );

  std::ostringstream msgStr;
  msgStr << "Oregano_Syn1588::disablePeriodicOut() OK:" << std::endl
      << "     EVENTCTRL: " << eventctrlStr.str();
  std::cout << msgStr.str() << std::endl;
}
// End of Oregano_Syn1588::disablePeriodicOut()


//------------------------- enableEventInterrupt() -----------------------------
//

void Oregano_Syn1588::enableEventInterrupt( const EEvent& who )
{
  std::ostringstream msgStr;

  if( who.isNoEvent() )
    return;

  InterruptBits iren( syn1588Ifc->readReg( Syn1588Ifc::IREN ) );

  msgStr << "enableEventInterrupt(" << who << "): Entry=0x"
    << std::hex << std::setfill('0') << std::setw(8) << iren.word;

  if( who.isEvent0() || who.isAllEvents() )
  {
    iren.field._10_1_event0 = 1;

    // Be sure we start with a clean FIFO
    syn1588->clearEvent0Fifo();
  }

  if( who.isEvent1() || who.isAllEvents()  )
  {
    iren.field._11_1_event1 = 1;
  }

  msgStr << ", Write=0x"
    << std::hex << std::setfill('0') << std::setw(8) << iren.word;

  // Enable event interrupt(s)
  syn1588Ifc->writeReg( Syn1588Ifc::IREN, iren.word );

  iren = syn1588Ifc->readReg( Syn1588Ifc::IREN );
  msgStr << ", Exit=0x"
    << std::hex << std::setfill('0') << std::setw(8) << iren.word;
  std::cout << msgStr.str() << std::endl;
}
// End of Oregano_Syn1588::enableEventInterrupt()


//----------------------- enableSquareWavePeriodicOut() ------------------------
//
void Oregano_Syn1588::enableSquareWavePeriodicOut(
  long double periodIn_s, long double startTime_s )
{
  long double halfPeriod( periodIn_s / 2.0 );
  EventCtrl eventctrl;                     // Maps the EVENTCTRL register bits
  PeriodTime periodTime0;                  // Maps PERIODTIMEn_H/L register bits
  TrigTime trigTime0;                      // Maps TRIGTIMEn_H/L register bits
  std::ostringstream eventctrlStr;

  // Configure period time with the half-period values...
  long double wholeSeconds = static_cast<long double>(static_cast<std::int64_t>(halfPeriod));
  long double subSeconds = (halfPeriod - wholeSeconds);
  long double nanoSeconds = subSeconds * 1000000000.0;
  long double wholeNano = static_cast<long double>( static_cast<std::uint64_t>(nanoSeconds));
  long double subNanoseconds = (nanoSeconds - wholeNano) * 10000.0;
  
  periodTime0.field.seconds = static_cast<std::uint64_t>( wholeSeconds );
  periodTime0.field.nanoseconds = static_cast<std::uint64_t>( nanoSeconds );
  periodTime0.field.subNanoseconds = static_cast<std::uint64_t>( subNanoseconds );
//  logger->Always("PERIODTIME0: SEC=%d, NANO=%d, SUB_NANO=%d, high=0x%08X, low=0x%08X\n",
//                  periodTime0.field.seconds, periodTime0.field.nanoseconds, periodTime0.field.subNanoseconds,
//                  periodTime0.reg.high, periodTime0.reg.low);

  // Configure trigger time with the supplied start time...
  wholeSeconds = static_cast<long double>(static_cast<std::int64_t>(startTime_s));
  subSeconds = (startTime_s - wholeSeconds);
  nanoSeconds = subSeconds * 1000000000.0;
  wholeNano = static_cast<long double>( static_cast<std::uint64_t>(nanoSeconds));
  subNanoseconds = (nanoSeconds - wholeNano) * 10000.0;

  trigTime0.field.seconds = static_cast<std::uint64_t>( wholeSeconds );
  trigTime0.field.nanoseconds = static_cast<std::uint64_t>( nanoSeconds );
  trigTime0.field.stateChange = 0;
//  logger->Always("TRIGTIME0: SEC=%d, NANO=%d, STATECHANGE=%d, high=0x%08X, low=0x%08X\n",
//                  trigTime0.field.seconds, trigTime0.field.nanoseconds, trigTime0.field.stateChange,
//                  trigTime0.reg.high, trigTime0.reg.low);

  // Set for square wave output. Period time registers are loaded with
  // 1/2 the desired output trigger period.
  syn1588Ifc->writeReg( Syn1588Ifc::PERIODTIME0_L,
      static_cast<std::uint32_t>(periodTime0.reg.low) );
  syn1588Ifc->writeReg( Syn1588Ifc::PERIODTIME0_H,
      static_cast<std::uint32_t>(periodTime0.reg.high) );

  // Clear all Event0, and Period0 control bits
  // We also want to assure that the PERIODTIME0 output is set to 0(low).
  // The TRIGGER0 function needs to be enabled before writing the start time.
  // Clear all Event0, Trigger0, and Period0 control bits and set TRIGGER0 before
  // writing the start time.
  eventctrl.word = syn1588Ifc->readReg( Syn1588Ifc::EVENTCTRL );

  eventctrlStr << "Entry=0x"
    << std::hex << std::setfill('0') << std::setw(8) << eventctrl.word;

  eventctrl.field._02_1_trigger0 = 1;       // enable before writing TRIGTIME0_L/H
  eventctrl.field._08_1_period0init = period0Sense.field._08_1_period0init;
  eventctrl.field._09_1_period0set = period0Sense.field._09_1_period0set;
  syn1588Ifc->writeReg( Syn1588Ifc::EVENTCTRL, eventctrl.word );

  eventctrlStr << "  Write #1=0x"
    << std::hex << std::setfill('0') << std::setw(8) << eventctrl.word;
  
  // Now write the trigger time
  syn1588Ifc->writeReg( Syn1588Ifc::TRIGTIME0_L,
      static_cast<std::uint32_t>(trigTime0.reg.low) );
  syn1588Ifc->writeReg( Syn1588Ifc::TRIGTIME0_H,
      static_cast<std::uint32_t>(trigTime0.reg.high) );

  // Enable event, trigger, and period 0
  // Leave period0_init/set alone!
  eventctrl.field._00_1_event0      =       // Enable event0 (Camera trigger out)
  eventctrl.field._01_1_event1      =       // Enable event1 (External signal in)
  eventctrl.field._04_1_period0     =       // Enable period0
  eventctrl.field._06_1_period0out  = 1;    // Enable period0 output
  eventctrl.field._15_2_period0strt = 3;    // Start period0 on trigger0
  syn1588Ifc->writeReg( Syn1588Ifc::EVENTCTRL, eventctrl.word );

  eventctrlStr << "  Write #2=0x"
    << std::hex << std::setfill('0') << std::setw(8) << eventctrl.word;

  eventctrlStr << "  Exit=0x" << std::hex << std::setfill('0') << std::setw(8)
    << syn1588Ifc->readReg( Syn1588Ifc::EVENTCTRL );

  std::ostringstream msgStr;
  msgStr.fill('0');
  msgStr << "Oregano_Syn1588::enableSquareWavePeriodicOut() OK" << std::endl
//      << "     startTime: " << startTime_s << std::endl
      << "        period: " << std::fixed << periodIn_s << "sec" << std::endl
      << "    halfPeriod: " << std::fixed << halfPeriod << "sec" << std::endl;
  msgStr.precision( 15 );
  msgStr
      << "       sub-sec: " << std::fixed << subSeconds << std::endl
      << "          nano: " << std::fixed << nanoSeconds << std::endl
      << "    whole-nano: " << std::fixed << wholeNano << std::endl
      << "      sub-nano: " << std::fixed << subNanoseconds << std::endl
      << "  PERIODTIME0_: " << periodTime0 << std::endl
      << "    TRIGTIME0_: " << trigTime0 << std::endl
      << "      IOMATRIX: 0x" << std::hex << std::setw(8) << iomTriggering.word << std::endl
      << "     EVENTCTRL: " << eventctrlStr.str();
  std::cout << msgStr.str() << std::endl;

}
// End of Oregano_Syn1588::enableSquareWavePeriodicOut()


//--------------------------- getSyn1588IfcPtr() -------------------------------
//
Syn1588Ifc* Oregano_Syn1588::getSyn1588IfcPtr()
{
  return syn1588Ifc;
}
// End of Oregano_Syn1588::getSyn1588IfcPtr()


//-------------------------------- getTime() -----------------------------------
//
Timestamp_t Oregano_Syn1588::getTime()
{
  Timestamp_t now;
  syn1588->getTime( now );
  return now;
}
// End of Oregano_Syn1588::getTime();


//----------------------------- waitForEvent() ---------------------------------
//

EEvent Oregano_Syn1588::waitForEvent( const int& timeout_ms  )
{
  // irsrc: 0x1==Event0, 0x2==Event1, 0x3==Event0 and Event1
  InterruptBits irsrc = syn1588Ifc->waitForEvent( timeout_ms );

  return EEvent(
      static_cast<bool>(irsrc.field._10_1_event0),
      static_cast<bool>(irsrc.field._11_1_event1) );
}
// End of Oregano_Syn1588::waitForEvent(


//----------------------- Oregano_Syn1588 Constructor --------------------------
//
Oregano_Syn1588::Oregano_Syn1588() :
  terminateEventMonitor( false ),  // Used by EventMonitor::doRun()
  syn1588Ifc(nullptr)
{
  // Instantiate Logger with default settings
  // PTP_ConfigLog cfglog;
  // cfglog.logLevel = LOG_LVL;
  logger = ptp::Logging::getRootLogger();
  logger->setLevel( ptp::log::Level::debug );

  // create syn1588 object
  syn1588.reset( new Syn1588( CARD_NUM ) );

  if (!syn1588->present()) {
    std::cout << "Error: Could not find syn1588(R) Hardware\n";
    exit(-1);
  }

  // Make short-cut for talking to registers
  syn1588Ifc = syn1588->getSyn1588Ifc();

  //------------------- Rate/Step Setup
  // Make certain step registers match clock frequency
  TimeInterval_t drift;
  drift.scaledNs = 0;
  syn1588->initRate( drift, 0 );

  // Create IOMATRIX reset value
  std::cout << "\nCreating IOMATRIX Default/Reset value...\n";
  iomReset.word = 0;                                        // Reset all values to 0
  iomReset.field._00_4_sma0X4 = Syn1588Ifc::IOM_SRC_1PPS;   // Driven by 1PPS
  iomReset.field._04_4_sma1X7 = Syn1588Ifc::IOM_SRC_PER0;   // Driven by Period 0
  iomReset.field._16_4_event0 = Syn1588Ifc::IOM_SRC_SMA2;   // Driven by connector X6
  iomReset.field._20_4_event1 = Syn1588Ifc::IOM_SRC_SMA1;   // Driven by connector X7

  std::cout << "Default/Reset " << iomToString(iomReset) << std::endl;

  std::cout << "Setting IOMATRIX with Default/Reset...\n";
  syn1588Ifc->writeReg( Syn1588Ifc::IOMATRIX, iomReset.word );  // Set it

  // Create IOMATRIX default configuration for triggering:
  // Triggers generated by period0 on SMA1 connector (X7)
  // Event 0 driven by SMA1 - to be used for timestamping actual trigger
  // No signal inversion at this time.
  //  SMA0 (X4) <-- 1PPS
  //  SMA1 (X7) <-- Period0
  //  SMA2 (X6) <-- SMA2
  //  Event0    <-- SMA1 (X7)
  //  Event1    <-- SMA2

  // Connect SMA0(X4) with 1PPS, SMA1(X7) with PERIOD0
  // Disable SMA2(X6) output (output tri-state, used as input)
  std::cout << "\nCreating IOMATRIX value for CSS Triggering...\n";
  iomTriggering.word = 0;
  iomTriggering.field._00_4_sma0X4 = Syn1588Ifc::IOM_SRC_1PPS;
  iomTriggering.field._04_4_sma1X7 = Syn1588Ifc::IOM_SRC_PER0;   // Trigger out to camera
  iomTriggering.field._08_4_sma2X6 = Syn1588Ifc::IOM_SRC_DIS;    // External signal in

  // Connect Event0 with PERIOD0
  // Connect Event1 with SMA2(X6)
  iomTriggering.field._16_4_event0 = Syn1588Ifc::IOM_SRC_PER0; // Camera trigger out
  iomTriggering.field._20_4_event1 = Syn1588Ifc::IOM_SRC_SMA2; // External signal in

  // Period sense is positive (active high). Used with EVENTCTRL
  // Period0 Set value is 0, Period0 initial value is 1.
  iomTriggering.field._25_1_level1     = 0; // active high
  period0Sense.field._08_1_period0init = 1; // Initial period0 value=1
  period0Sense.field._09_1_period0set  = 0; // Set period0 out to 0 when enabled
                                            // Also used to set to 0 when disabled

  std::cout << "CSS Triggering " << iomToString(iomTriggering) << std::endl;

  std::cout << "Disabling all events, triggers and periods...\n";

  // Start setting up registers....
  // Clear the EVENTCTRL 2x (as recommended in docs) to be sure all functions
  // are disabled...
  std::cout << "Clearing EVENTCTRL (twice)..." << std::endl;
  syn1588Ifc->writeReg( Syn1588Ifc::EVENTCTRL, 0 );
  syn1588Ifc->writeReg( Syn1588Ifc::EVENTCTRL, 0 );

  // Initialize PERIOD0 output for currently selected trigger sense.
  std::cout << "Setting EVENTCTRL="
    << std::hex << std::setfill('0') << std::setw(8) << period0Sense.word
    << " to set Period0 Sense." << std::endl;
  syn1588Ifc->writeReg( Syn1588Ifc::EVENTCTRL, period0Sense.word );

  // Set the IOMATRIX register
  syn1588Ifc->writeReg( Syn1588Ifc::IOMATRIX, iomTriggering.word );

  // Verify it took...
  IOMatrix tmp;
  tmp.word = syn1588Ifc->readReg( Syn1588Ifc::IOMATRIX );
  if( tmp.word != iomTriggering.word )
  {
    std::ostringstream errMsg;
    errMsg << std::hex << std::setfill('0');
    errMsg << "Failure setting/verifying IOMATRIX value: "
           << "expectedValue=0x" << std::setw(8) << iomTriggering.word
           << ", actualValue=0x" << std::setw(8) << tmp.word;
    std::cout << errMsg.str() << std::endl;
    exit(1);
  }
}
// End of Oregano_Syn1588 Constructor


//----------------------- Oregano_Syn1588 Destructor ---------------------------
//
Oregano_Syn1588::~Oregano_Syn1588( )
{
  std::cout << "Oregano_Syn1588 Destructor..." << std::endl;

  if( syn1588Ifc != nullptr )
  {
    // Disable all interrupts
    std::cout << "    Setting IREN=0..." << std::endl;
    syn1588Ifc->writeReg( Syn1588Ifc::IREN, 0 );
    std::cout << "      IREN=0x"
      << std::hex << std::setfill('0') << std::setw(8)
      << syn1588Ifc->readReg( Syn1588Ifc::IREN ) << std::endl;

    // Disable EVENTn, TRIGGERn, PERIODTIMEn functions
    // First write disables the functions
    std::cout << "    Setting EVENTCTRL=0 (Write 1 of 2)..." << std::endl;
    syn1588Ifc->writeReg( Syn1588Ifc::EVENTCTRL, 0 );
    std::cout << "      EVENTCTRL=0x"
      << std::hex << std::setfill('0') << std::setw(8)
      << syn1588Ifc->readReg( Syn1588Ifc::EVENTCTRL ) << std::endl;

    // Second write returns PERIOD0/1 init/set values to 0. These can only be
    // changed if PERIOD0/1 is disabled or at the same time it is being enabled...
    std::cout << "    Setting EVENTCTRL=0 (Write 2 of 2)..." << std::endl;
    syn1588Ifc->writeReg( Syn1588Ifc::EVENTCTRL, 0 );
    std::cout << "      EVENTCTRL=0x"
      << std::hex << std::setfill('0') << std::setw(8)
      << syn1588Ifc->readReg( Syn1588Ifc::EVENTCTRL ) << std::endl;

    // Reset the IOMATRIX register
    std::cout << "    Setting IOMATRIX=0x"
      << std::hex << std::setfill('0') << std::setw(8)
      << iomReset.word << "..." << std::endl;
    syn1588Ifc->writeReg( Syn1588Ifc::IOMATRIX, iomReset.word );
    std::cout << "      IOMATRIX=0x"
      << std::hex << std::setfill('0') << std::setw(8)
      << syn1588Ifc->readReg( Syn1588Ifc::IOMATRIX ) << std::endl;
  }

  std::cout << "Destructor complete!" << std::endl;

  syn1588Ifc = nullptr;                   // Clear convenience pointer
}
// End of Oregano_Syn1588 Destructor

}  // End of namespace test




//==============================================================================
// Event Monitor
//

void eventMonitor(std::shared_ptr<Oregano_Syn1588> oregano, int timeout_ms)
{
  bool firstTime(true);
  EEvent who;                    // Result from waitForEvent
  Syn1588Ifc *syn1588Ifc( oregano->getSyn1588IfcPtr() );
  std::ostringstream msgStr;

  msgStr << "eventMonitor using event timeout of " << timeout_ms << "ms";
  std::cout << msgStr.str() << std::endl;

  event0occurred = false;
  eventMonitorRunning = true;

  while( true )
  {
    if( firstTime )
    {
      std::cout << "eventMonitor running...\n";
      firstTime = false;
    }

    if( terminateEventMonitor ) break;

    who = oregano->waitForEvent( timeout_ms );

    if( who.isNoEvent() )
    {
      if( terminateEventMonitor )
        break;
      else
      {
        std::cout << "Timeout waiting " << timeout_ms << "ms for TRADS event!\n";
        continue;
      }
    }


    // If available, read the timestamp from the event.
    if( who.isEvent0() || who.isAllEvents() )
    {
      // This is the latched time of the camera trigger being generated by
      // the TRADS card.

      bool isZero(false);
      // Read FIFO till empty, log each non-zero timestamp
      do {
        Timestamp_t eventTime0;
        eventTime0.nsec  = syn1588Ifc->readReg( Syn1588Ifc::EVENTTIME0_L );
        eventTime0.sec_l = syn1588Ifc->readReg( Syn1588Ifc::EVENTTIME0_H );
        eventTime0.sec_h = 0;
        if( eventTime0.nsec == 0 && eventTime0.sec_l == 0 && eventTime0.sec_h == 0 )
          isZero = true;
        else
        {
          std::ostringstream msgStr("    Event0 @ ", std::ios_base::ate);
          msgStr << std::setfill('0');
          msgStr << "H=0x" << std::hex << std::setw(8)<< eventTime0.sec_h
            << ", L=0x" << std::hex << std::setw(8) << eventTime0.sec_l
            << ", NS=0x" << std::hex << std::setw(8) << eventTime0.nsec << "\n";
          std::cout << msgStr.str();
        }
      } while( !isZero );
      event0occurred = true;
    }

    // If available, read the timestamp from the event.
    if( who.isEvent1() || who.isAllEvents() )
    {
      // This is the latched time of some auxiliary input signal.
      // Typically, this will be the camera strobe output and will be used
      // for testing purposes.

      Timestamp_t eventTime1;
      eventTime1.nsec  = syn1588Ifc->readReg( Syn1588Ifc::EVENTTIME1_L );
      eventTime1.sec_l = syn1588Ifc->readReg( Syn1588Ifc::EVENTTIME1_H );
      eventTime1.sec_h = 0;

      std::ostringstream msgStr("    Event1 @ ", std::ios_base::ate);
      msgStr << std::setfill('0');
      msgStr << "H=0x" << std::hex << std::setw(8)<< eventTime1.sec_h
        << ", L=0x" << std::hex << std::setw(8) << eventTime1.sec_l
        << ", NS=0x" << std::hex << std::setw(8) << eventTime1.nsec << "\n";
      std::cout << msgStr.str();
    }

    // Clear interrupt
    oregano->clearEventInterrupt( who );
  }

  std::cout << "Event monitor stopped...\n";
};
// End of Event Monitor
//==============================================================================


int main (int argc, char **argv) {
  std::cout << "\n********** INSTANTIATING Oregano_Syn1588() OBJECT\n*\n";

  std::shared_ptr<Oregano_Syn1588> oregano;
  oregano = std::make_shared<Oregano_Syn1588>();

  std::cout << "*\n********** INSTANTIATION OF Oregano_Syn1588() COMPLETE\n";

  EEvent who = EEvent::Event0();

  // Start up the event monitor thread.
  // For good measure I'm adding an additional 1000ms to the event timeout.
  std::cout << "\n********** STARTING EVENT MONITOR\n";
  int timeout_ms = (PERIOD_TIME * 1000) + 1000;
  std::thread eventMonitorThread( eventMonitor, oregano, timeout_ms );
  while( !eventMonitorRunning );   // Yes, this shouldn't be infinite!

  // Disable interrupts
  std::cout << "\n********** DISABLING ALL EVENT INTERRUPTS\n";
  oregano->disableEventInterrupt( EEvent::AllEvents() );

  //  - Enable period0 to start at trigtime0.
  //  - wait for a desired start time, in this example an integral second.
  //  - wait for the event to occur which should be coincident with the trigger output.
  //  - delay for some period time which simulates an exposure time.

  std::cout << "\n********** ENABLING SQUARE WAVE OUTPUT\n";

  // To make things easy to read, start on next one-second boundary.
  Timestamp_t now( oregano->getTime() );
  Timestamp_t start {0,0,0};
  start.sec_l = now.sec_l + 1;

  oregano->enableSquareWavePeriodicOut( PERIOD_TIME, start.sec_l );

  // Enable Event0 interrupt. This method also clears the Event0 FIFO
  std::cout << "\n********** ENABLING EVENT INTERRUPT\n";
  oregano->enableEventInterrupt( who );

  std::cout
    << "\n********** WATIING FOR " << NUM_EVENTS_TO_WAIT_FOR << " EVENTS...\n"
    << "Expected Event Interval: " << PERIOD_TIME << "s\n";

  int maxWait_ms = (PERIOD_TIME * 2.0) * 1000.0;   // Max time to wait for event
  int exposure_time_ms = EXPOSURE_TIME * 1000;
  auto exposure_time = std::chrono::milliseconds( exposure_time_ms );

  int eventCount(0);
  for( int i=0; i<NUM_EVENTS_TO_WAIT_FOR; i++ )
  {
    auto start = std::chrono::steady_clock::now();

    // Wait for "trigger". We expect it once every PERIOD_TIME seconds...
    while( !event0occurred )
    {
      auto curr = std::chrono::steady_clock::now();
      int dt = std::chrono::duration<double, std::milli>(curr-start).count();
      if( dt >= maxWait_ms )
      {
        std::cout << "ERROR: Timeout waiting "
          << std::fixed << maxWait_ms/1000.0 << "sec for"
          << " event configured to occur every " << PERIOD_TIME << "sec!\n";
        break;
      }

    }

    if( event0occurred )
    {
      eventCount++;
      std::this_thread::sleep_for( exposure_time );  // Simulate exposure time

      event0occurred = false;                    // Reset for next event
    }
  }

  if( eventCount == NUM_EVENTS_TO_WAIT_FOR )
    std::cout << "SUCCESS: " << NUM_EVENTS_TO_WAIT_FOR
      << " events detected, terminating test...\n";
  else
    std::cout << "FAILURE: " << NUM_EVENTS_TO_WAIT_FOR
      << " events were NOT detected, terminating test...\n";

  // Disable Event0 interrupt and periodic output
  // NOTE: With the way my disable is written, this will force PERIOD0 output
  //       to 0. On the o-scope it will look like a 20ms pulse but in fact it
  //       is a square wave that has been disabled before the time in PERIOD0
  //       has expired.
  std::cout << "\n********** DISABLING EVENT INTERRUPT\n";
  oregano->disableEventInterrupt( who );

  std::cout << "\n********** DISABLING SQUARE WAVE OUTPUT\n";
  oregano->disablePeriodicOut();

  std::cout << "\n********** TERMNATING EVENT MONITOR\n";
  terminateEventMonitor = true;
  eventMonitorThread.join();

  std::cout << "\n********** DELETING Oregano_1588() OBJECT...\n";
  oregano.reset();

  return 0;
}

/*
// =============================================================================
// LR workaround

// start a very long period on period0 immediately (without trigger)
void startLongPeriod() {
  std::ostringstream msgStr;

  uint32_t periodtimeH = 0xEFFFFFFF; // very long period
  uint32_t periodtimeL = 0x0;

  //set periodtime
  syn1588->getSyn1588Ifc()->writeReg(Syn1588Ifc::PERIODTIME0_L, periodtimeL);
  syn1588->getSyn1588Ifc()->writeReg(Syn1588Ifc::PERIODTIME0_H, periodtimeH);

  // enable period0
  uint32_t eventctrl = syn1588->getSyn1588Ifc()->readReg(Syn1588Ifc::EVENTCTRL);

  // DCB Fix for errant edge:
  // PERIOD0 OUTPUT needs to be disabled as it still asserts when period0 is enabled
  // even though eventctrl(8) and eventctrl(9) are both 0.
//  eventctrl |= (0x1 << 4 |      // enable period0
//                0x1 << 6);      // enable period0out
//  eventctrl &= ~(0x3 << 15);    // start period0 now (no trigger: remove bits in case they were set before)

  eventctrl |= (0x1 << 4);      // enable period0
  eventctrl &= ~(0x1 << 6) &    // disable period0out
               ~(0x3 << 15);    // start period0 now (no trigger: remove bits in case they were set before)
  // END DCB

  syn1588->getSyn1588Ifc()->writeReg(Syn1588Ifc::EVENTCTRL, eventctrl);

  msgStr << "startedLongPeriod() - EVENTCTRL: "
         << std::hex << std::setw(8) << std::setfill('0') << eventctrl << "\n";
  std::cout << msgStr.str();

}

// disable the current period and set both current period0out and initial period0out to 0
void disablePeriod() {
  std::ostringstream msgStr;

  uint32_t eventctrl = syn1588->getSyn1588Ifc()->readReg(Syn1588Ifc::EVENTCTRL);

  eventctrl &= (~(0x1 << 4) &   // disable period0
                ~(0x1 << 6));   // disable period0out

  syn1588->getSyn1588Ifc()->writeReg(Syn1588Ifc::EVENTCTRL, eventctrl);

  msgStr << "disablePeriod() - EVENTCTRL: "
         << std::hex << std::setw(8) << std::setfill('0') << eventctrl << "\n";
  std::cout << msgStr.str();


  eventctrl &= ( ~(0x1 << 8) &
                 ~(0x1 << 9));  // set initVal=0 & currentVal=0

  syn1588->getSyn1588Ifc()->writeReg(Syn1588Ifc::EVENTCTRL, eventctrl);

  msgStr.str("");
  msgStr << "disablePeriod() -  EVENTCTRL: "
         << std::hex << std::setw(8) << std::setfill('0') << eventctrl << "\n";
  std::cout << msgStr.str();

}

// stop the long workaround-period
void stopPeriod() {
  std::ostringstream msgStr;

  uint32_t eventctrl = syn1588->getSyn1588Ifc()->readReg(Syn1588Ifc::EVENTCTRL);

  eventctrl &= (~(0x1 << 4) &   // disable period0
                ~(0x1 << 6));   // disable period0out

  syn1588->getSyn1588Ifc()->writeReg(Syn1588Ifc::EVENTCTRL, eventctrl);

  msgStr << "stopPeriod() - EVENTCTRL: "
         << std::hex << std::setw(8) << std::setfill('0') << eventctrl << "\n";
  std::cout << msgStr.str();
}
// end of LR functions
//==============================================================================
*/
