/*
 * $Id: EEvent.cpp,v 1.3 2023/03/20 19:19:17 cberst Exp $
 *
 * Copyright 2016 Daniel K. Inouye Solar Telescope,
 * National Solar Observatory, operated by the Association of Universities
 * for Research in Astronomy, Inc., under cooperative agreement with the
 * National Science Foundation.
 *
 * This copy of DKIST software is licensed to you under the terms
 * described in the ATST_LICENSE file included in this distribution.
 *
 * Created: Aug 4 2017
 *
 * Author: Chris Berst (cberst@nso.edu)
 */

#include "EEvent.h"

namespace test {

//==============================================================================
// CLASS: EEvent
//==============================================================================

//------------------------------------------------------------------------------
// CamMode: PUBLIC
//------------------------------------------------------------------------------

//============================= CONSTRUCTOR #1 =================================
//
EEvent::EEvent() :
    enumValue( EnumEvent::_NO_EVENTS ) {;}
// End of Constructor #1


//============================= CONSTRUCTOR #2 =================================
//
EEvent::EEvent(const EEvent& event)
{
  *this = event;
}
// End of Constructor #2


#ifdef TRADS_TRIGGERING
//============================= CONSTRUCTOR #3 =================================
//
EEvent::EEvent(const std::uint32_t who)
{
  enumValue = static_cast<EnumEvent>(
      who & (Syn1588Ifc::EVCB_EVENT0 | Syn1588Ifc::EVCB_EVENT1) );
}
// End of Constructor #3
#endif


//============================= CONSTRUCTOR #4 =================================
//
EEvent::EEvent( const bool event0, const bool event1 )

{
  if( event0 && event1 )
    enumValue = EnumEvent::_ALL_EVENTS;
  else if( event0 )
    enumValue = EnumEvent::_EVENT_0;
  else if( event1 )
    enumValue = EnumEvent::_EVENT_1;
  else
    enumValue = EnumEvent::_NO_EVENTS;
}
// End of Constructor #4


//------------------------------- operator=() ----------------------------------
// Assigns rhs to the contents of this object.
//
EEvent& EEvent::operator=( const EEvent& rhs )
{
  if( this == &rhs )            // check for assignment to self
    return *this;

  this->enumValue = rhs.enumValue;

  return *this;
}


//------------------------------- operator<<() ---------------------------------
// Inserts this object onto an output stream in human readable form.
//
std::ostream& operator<<( std::ostream& os, const EEvent& rhs )
{
  os << rhs.enumValue;
  return os;
}
// End of operator<<(EEvent)


}  // End of namespace test

//------------------------------------------------------------------------------
// END OF FILE: EEvent.h
//------------------------------------------------------------------------------
