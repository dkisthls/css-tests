#ifndef SYN1588_NO_TRIGGER_OUT_H_
#define SYN1588_NO_TRIGGER_OUT_H_

#include "EEvent.h"

#include <cstdint>
#include <memory>                     // std::unique_ptr
#include <ostream>                    // std::ostream

#include "syn1588.h"

namespace test {

/**
 * Bitfield that describes the syn1588 IO Matrix Control Register (IOMATRIX).
 * The "source" for each field may be one of the Syn1588Ifc::IOM_SRC_XXX constant
 * values defined in syn1588_ifc.h.
 * The IOMATRIX register contains the mapping of a source (i.e. an IOM_SRC_XXX
 * value) to it's destination (a field in this bitfield).
 * Level definitions for SMA connectors 0, 1, 2, 3 (X4,X7,X6,X5) are:
 *   0 = active high\n
 *   1 = active low (inverted internally)\n
 * Naming Convention: _<starting bit #>_<# bits>_name
 *
 * This bitfield mapping is for little-endian systems.
 *
 */
typedef union IOMatrix {
  std::uint32_t word;
  struct {
    std::uint32_t _00_4_sma0X4 : 4;    // Source for SMA connector 0 output (X4) (3:0)
    std::uint32_t _04_4_sma1X7 : 4;    // Source for SMA connector 1 output (X7) (7:4)
    std::uint32_t _08_4_sma2X6 : 4;    // Source for SMA connector 2 output (X6) (11:8)
    std::uint32_t _12_4_sma3X5 : 4;    // Source for SMA connector 3 output (X5) (15:12)
    std::uint32_t _16_4_event0 : 4;    // Source for event 0 input of syn1588    (19:16)
    std::uint32_t _20_4_event1 : 4;    // Source for event 1 input of syn1588    (23:20)
    std::uint32_t _24_1_level0 : 1;    // Level for SMA connector 0 input (x4)   (24)
    std::uint32_t _25_1_level1 : 1;    // Level for SMA connector 1 input (x7)   (25)
    std::uint32_t _26_1_level2 : 1;    // Level for SMA connector 2 input (x6)   (26)
    std::uint32_t _27_1_level3 : 1;    // Level for SMA connector 3 input (x5)   (27)
    std::uint32_t              : 4;    // RESERVED                               (31:28)
  } field;
} IOMatrix;


typedef union EventCtrl {
  std::uint32_t word;
  struct {
    std::uint32_t _00_1_event0      : 1;    // 1 enables EVENT0 function            (0)
    std::uint32_t _01_1_event1      : 1;    // 1 enables EVENT1 function            (1)
    std::uint32_t _02_1_trigger0    : 1;    // 1 enables TRIGGER0 function          (2)
    std::uint32_t _03_1_trigger1    : 1;    // 1 enables TRIGGER1 function          (3)
    std::uint32_t _04_1_period0     : 1;    // 1 enables PERIODTIME0 function       (4)
    std::uint32_t _05_1_period1     : 1;    // 1 enables PERIODTIME1 function       (5)
    std::uint32_t _06_1_period0out  : 1;    // 1 enables PERIODTIME0 output         (6)
    std::uint32_t _07_1_period1out  : 1;    // 1 enables PERIODTIME1 output         (7)
    std::uint32_t _08_1_period0init : 1;    // Initial value of PERIODTIME0 output  (8)
    std::uint32_t _09_1_period0set  : 1;    // Set PERIODTIME0 output to this value (9)
    std::uint32_t _10_1_period1init : 1;    // Initial value of PERIODTIME1 output  (10)
    std::uint32_t _11_1_period1set  : 1;    // Set PERIODTIME1 output to this value (11)
    std::uint32_t _12_1_irigb       : 1;    // 1 enables IRIG-B output              (12)
    std::uint32_t _13_1_period0pdc  : 1;    // 1 enables PERIODTIME0 programmable duty cycle (13)
    std::uint32_t                   : 1;    // RESERVED                             (14)
    std::uint32_t _15_2_period0strt : 2;    // Start PERIODTIME0 function on...     (16:15)
    std::uint32_t _17_2_period1strt : 2;    // Start PERIODTIME1 function on...     (18:17)
    std::uint32_t                   : 13;   // RESERVED                             (31:19)
  } field;
} EventCtrl;


// Register definitions for IRSRC and IREN are identical. A single class can be
// used for both.
class InterruptBits {
  public:
    union {
      std::uint32_t word;
      struct {
        std::uint32_t _00_1_mitts0_rx       : 1;  // MITTS0_RX created a Timestamp (0)
        std::uint32_t _01_1_tsfif0o_rx_full : 1;  // 1 if TSFIFO0_RX became full   (1)
        std::uint32_t _02_1_mitts0_tx       : 1;  // MIITS0_TX created a Timestamp (2)
        std::uint32_t _03_1_tsfifo0_tx_full : 1;  // 1 if TSFIFO0_tX became full   (3)
        std::uint32_t _04_1_mitts1_rx       : 1;  // MITTS1_RX created a Timestamp (4)
        std::uint32_t _05_1_tsfif1o_rx_full : 1;  // 1 if TSFIFO1_RX became full   (5)
        std::uint32_t _06_1_mitts1_tx       : 1;  // MIITS1_TX created a Timestamp (6)
        std::uint32_t _07_1_tsfifo1_tx_full : 1;  // 1 if TSFIFO1_tX became full   (7)
        std::uint32_t                       : 2;  // N/A                           (9:8)
        std::uint32_t _10_1_event0          : 1;  // EVENT0 occurred if 1          (10)
        std::uint32_t _11_1_event1          : 1;  // EVENT1 occurred if 1          (11)
        std::uint32_t _12_1_trigger0        : 1;  // TRIGGER0 matched if 1         (12)
        std::uint32_t _13_1_trigger1        : 1;  // TRIGGER1 matched if 1         (13)
        std::uint32_t                       : 10; // N/A                           (23:14)
        std::uint32_t _24_1_periodTime0     : 1;  // 1 if time of PERIODTIME0 has elapsed (24)
        std::uint32_t _25_1_periodTime1     : 1;  // 1 if time of PERIODTIME0 has elapsed (25)
        std::uint32_t                       : 5;  // N/A                           (30:26)
        std::uint32_t _31_1_timeOverflow    : 1;  // 1 if TIME overflowed          (31)
      } field;
    };

  public:
    InterruptBits() : word(0) {}
    InterruptBits( const std::uint32_t value ) { word = value; }

    test::InterruptBits& operator=( const test::InterruptBits& rhs);
};

class PeriodTime {
  public:
    union {
      struct {
        std::uint64_t subNanoseconds : 16;
        std::uint64_t nanoseconds    : 30;
        std::uint64_t                : 2;
        std::uint64_t seconds        : 16;
      } field;
      struct {
        std::uint64_t low  : 32;
        std::uint64_t high : 32;
      } reg;
    };

    PeriodTime() { reg.low = reg.high = 0; }

    test::PeriodTime& operator=( const test::PeriodTime& rhs);

    friend std::ostream& operator<<(
      std::ostream& os, const test::PeriodTime& rhs );
};

class TrigTime {
  public:
    union {
      struct {
        std::uint64_t nanoseconds    : 32;
        std::uint64_t seconds        : 20;
        std::uint64_t stateChange    : 2;
        std::uint64_t                : 10;
      } field;
      struct {
        std::uint64_t low  : 32;
        std::uint64_t high : 32;
      } reg;
    };

    TrigTime() { reg.low = reg.high = 0; }

    test::TrigTime& operator=( const test::TrigTime& rhs);

    friend std::ostream& operator<<(
      std::ostream& os, const test::TrigTime& rhs );
};


class Oregano_Syn1588 {
  private:
    bool terminateEventMonitor;  // Used by EventMonitor::doRun()

    test::IOMatrix iomReset;
    test::IOMatrix iomTriggering;
    test::EventCtrl period0Sense;

    // syn1588 Object and logger
    std::unique_ptr<::Syn1588> syn1588;
    Syn1588Ifc* syn1588Ifc;              // Convenience pointer from syn1588

    // PTP Shared Memory logging object...
    std::shared_ptr<ptp::Logger> logger;

    std::string iomDecodeSource( const std::uint8_t src );
    std::string iomToString( const IOMatrix& iom );

  public:
    Oregano_Syn1588();
    ~Oregano_Syn1588();

    void clearEventInterrupt( const test::EEvent& who );

    void disableEventInterrupt( const test::EEvent& who );

    void disablePeriodicOut();

    void enableEventInterrupt( const EEvent& who );

    Syn1588Ifc* getSyn1588IfcPtr();

    Timestamp_t getTime();

    void enableSquareWavePeriodicOut(
      long double periodIn_s, long double startTime_s );

    test::EEvent waitForEvent( const int& timeout_ms );
};

}  // End of namespace test

#endif
