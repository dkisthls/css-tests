/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2016
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 */

/// syn1588_example_trigger_period
///
/// This simple examples shows how to generate periodic or one-time events
/// using the syn1588 hardware. A 10 Hz output signal is generated and routed
/// to an output pin.
/// One-time events are generated using the TRIGGER resources, periodic events
/// are generated using the PERIOD resources. Both types may be generated using
/// the generateEvent API function.

#include "ptp/log.hpp"
#include "syn1588.h"

#include <atomic>
#include <chrono>
#include <iomanip>
#include <iostream>
#include <stdio.h>
#include <thread>

#ifdef WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

using ptp::Logger;

static const char STD_BANNER[] = "syn1588(R) Demo - " __DATE__ "\n"
"Copyright (c) Oregano Systems - Design & Consulting GesmbH 2005-2016\n"
"Confidential unpublished data - All rights reserved\n\n";


#define CARD_NUM 0           // card number 0 is the first found syn1588 card.

#define PERIOD_TIME 1.0      // in seconds
#define EXPOSURE_TIME 0.020  // in seconds
#define LOG_LVL ptp::log::Level::debug

// Factory IOMATRIX reset value
uint32_t iomReset = ((Syn1588Ifc::IOM_SRC_1PPS << 0 ) |    // SMA0 driven by 1PPS
                     (Syn1588Ifc::IOM_SRC_PER0 << 4 ) |    // SMA1 driven by Period 0
                     (Syn1588Ifc::IOM_SRC_SMA2 << 16) |    // Event0 driven by connector X6
                     (Syn1588Ifc::IOM_SRC_SMA1 << 20));    // Event1 driven by connector X7

// IOMATRIX
// Connect SMA0(X4) with 1PPS, SMA1(X7) with PERIOD0
// Disable SMA2(X6) output (output tri-state, used as input)
// Connect Event0 with PERIOD0
// Connect Event1 with SMA2(X6)
// NOTE: Athough connected, for this example I am not actually enabling and monitoring Event1.
uint32_t iomTriggering = ((Syn1588Ifc::IOM_SRC_1PPS << 0 ) |  // SMA0 driven by 1PPS
                          (Syn1588Ifc::IOM_SRC_PER0 << 4 ) |  // SMA1 driven by Period0, Trigger out to camera
                          (Syn1588Ifc::IOM_SRC_DIS  << 8 ) |  // SMA2 output disabled, used as aux external signal in
                          (Syn1588Ifc::IOM_SRC_PER0 << 16) |  // Event0 driven by Period0 (i.e. Trigger out to camera)
                          (Syn1588Ifc::IOM_SRC_SMA2 << 20) |  // Event1 driven by SMA2 (Aux external signal in monitor)
                          (1 << 25));                         // SMA1 Level 


typedef union PeriodTime {
  uint64_t word;
  struct {
    std::uint64_t subNanoseconds : 16;
    std::uint64_t nanoseconds    : 30;
    std::uint64_t                : 2;
    std::uint64_t seconds        : 16;
  } field;
  struct {
    std::uint64_t low  : 32;
    std::uint64_t high : 32;
  } reg;
} PeriodTime;

typedef union TrigTime {
  uint64_t word;
  struct {
    std::uint64_t nanoseconds    : 32;
    std::uint64_t seconds        : 20;
    std::uint64_t stateChange    : 2;
    std::uint64_t                : 10;
  } field;
  struct {
    std::uint64_t low  : 32;
    std::uint64_t high : 32;
  } reg;
} TrigTime;



// Period sense is positive (active high). Used with EVENTCTRL
// Period0 Set value is 0, Period0 initial value is 1.
uint32_t period0Sense = 0x00000100;                    // Period0 initial value

// syn1588 Object and logger
std::shared_ptr<ptp::Logger> logger;      // PTP Shared Memory logging object...
std::unique_ptr<::Syn1588> syn1588;
std::atomic_bool terminateEventMonitor;
std::atomic_bool event0occurred;

// =============================================================================
// LR workaround

// start a very long period on period0 immediately (without trigger)
void startLongPeriod() {

  uint32_t periodtimeH = 0xEFFFFFFF; // very long period
  uint32_t periodtimeL = 0x0;
  
  //set periodtime
  syn1588->getSyn1588Ifc()->writeReg(Syn1588Ifc::PERIODTIME0_L, periodtimeL);
  syn1588->getSyn1588Ifc()->writeReg(Syn1588Ifc::PERIODTIME0_H, periodtimeH);

  // enable period0
  uint32_t eventctrl = syn1588->getSyn1588Ifc()->readReg(Syn1588Ifc::EVENTCTRL);

  // DCB Fix for errant edge:
  // PERIOD0 OUTPUT needs to be disabled as it still asserts when period0 is enabled
  // even though eventctrl(8) and eventctrl(9) are both 0.
//  eventctrl |= (0x1 << 4 |      // enable period0
//                0x1 << 6);      // enable period0out
//  eventctrl &= ~(0x3 << 15);    // start period0 now (no trigger: remove bits in case they were set before)

  eventctrl |= (0x1 << 4);      // enable period0
  eventctrl &= ~(0x1 << 6) &    // disable period0out
               ~(0x3 << 15);    // start period0 now (no trigger: remove bits in case they were set before)
  // END DCB

  syn1588->getSyn1588Ifc()->writeReg(Syn1588Ifc::EVENTCTRL, eventctrl);

  logger->Always("started period: EVENTCTRL: %08X\n", eventctrl);

}

// disable the current period and set both current period0out and initial period0out to 0
void disablePeriod() {

  uint32_t eventctrl = syn1588->getSyn1588Ifc()->readReg(Syn1588Ifc::EVENTCTRL);

  eventctrl &= (~(0x1 << 4) &   // disable period0
                ~(0x1 << 6));   // disable period0out

  syn1588->getSyn1588Ifc()->writeReg(Syn1588Ifc::EVENTCTRL, eventctrl);
  logger->Always("disabled period: EVENTCTRL: %08X\n", eventctrl);

  eventctrl &= ( ~(0x1 << 8) &
                 ~(0x1 << 9));  // set initVal=0 & currentVal=0

  syn1588->getSyn1588Ifc()->writeReg(Syn1588Ifc::EVENTCTRL, eventctrl);
  logger->Always("disabled period: EVENTCTRL: %08X\n", eventctrl);

}

// stop the long workaround-period
void stopPeriod() {

  uint32_t eventctrl = syn1588->getSyn1588Ifc()->readReg(Syn1588Ifc::EVENTCTRL);

  eventctrl &= (~(0x1 << 4) &   // disable period0
                ~(0x1 << 6));   // disable period0out

  syn1588->getSyn1588Ifc()->writeReg(Syn1588Ifc::EVENTCTRL, eventctrl);
  logger->Always("stopped period: EVENTCTRL: %08X\n", eventctrl);
}
// end of LR functions
//==============================================================================

//==============================================================================
// Class: Event Monitor
//

void eventMonitor()
{
  event0occurred = false;
  while( !terminateEventMonitor )
  {
    std::uint32_t irsrc = syn1588->getSyn1588Ifc()->waitForEvent( 1000 );
    if( !irsrc ) continue;

    if( irsrc & 0x00000400l )
    {
      bool isZero(false);
      // Read FIFO till empty, log each non-zero timestamp
      do {              
        Timestamp_t eventTime0;
        eventTime0.nsec  = syn1588->getSyn1588Ifc()->readReg( Syn1588Ifc::EVENTTIME0_L );
        eventTime0.sec_l = syn1588->getSyn1588Ifc()->readReg( Syn1588Ifc::EVENTTIME0_H );
        eventTime0.sec_h = 0;
        if( eventTime0.nsec == 0 && eventTime0.sec_l == 0 && eventTime0.sec_h == 0 )
          isZero = true;
        else
//          logger->Always("     Event0 @ %d%d,%09d\n\n", eventTime0.sec_h, eventTime0.sec_l, eventTime0.nsec);
        std::cout << "    Event0 @ H=0x"
          << std::hex << std::setfill('0') << std::setw(8)<< eventTime0.sec_h
          << ", L=0x" << eventTime0.sec_l
          << ", NS=0x" << eventTime0.nsec << "\n";
      } while( !isZero );
      event0occurred = true;
    }
 
    if( irsrc & 0x00000800l )
    {
      syn1588->getSyn1588Ifc()->readReg( Syn1588Ifc::EVENTTIME1_L );
      syn1588->getSyn1588Ifc()->readReg( Syn1588Ifc::EVENTTIME1_H );
    }
  
    // Clear interrupt
    syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::IRSRC, ~irsrc );
  }
};

// End of Class: Event Monitor
//==============================================================================


//==============================================================================
// UTILITY METHODS

// Utility method to wait for some number of milliseconds
void delay(double time_s )
{
  int dt;
  int delay_ms = (time_s * 1000.0);
  auto start = std::chrono::steady_clock::now();
  do {
    auto curr = std::chrono::steady_clock::now();
    dt = std::chrono::duration<double, std::milli>(curr-start).count();
  } while( dt < delay_ms );
}
// End of delay()


//-------------------------- disablePeriodicOut() -------------------------------
//
void disablePeriodicOut()
{
  // Disable all output
  syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::EVENTCTRL, 0 );

  // Reset period0 sense
  // Note: Disabling of periodic output is asynchronous and could occur when
  // the output is either asserted or de-asserted. If asserted then we need to
  // return it to the de-asserted state.
  syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::EVENTCTRL, period0Sense );
}
// End of disablePeriodicOut()


//----------------------- enableSquareWavePeriodicOut() ------------------------
//
void enableSquareWavePeriodicOut( long double periodIn_s, long double startTime_s )
{
  long double halfPeriod( periodIn_s / 2.0 );
  uint32_t eventCtrl; 
  PeriodTime periodTime0;    // Maps PERIODTIMEn_H/L parts to register bits
  TrigTime   trigTime0;      // Maps TRIGTIMEn_H/L parts to register bits

  // Configure period time with the half-period values...
  long double wholeSeconds = static_cast<long double>(static_cast<std::int64_t>(halfPeriod));
  long double subSeconds = (halfPeriod - wholeSeconds);
  long double nanoSeconds = subSeconds * 1000000000.0;
  long double wholeNano = static_cast<long double>( static_cast<std::uint64_t>(nanoSeconds));
  long double subNanoseconds = (nanoSeconds - wholeNano) * 10000.0;
  
  periodTime0.word = 0;
  periodTime0.field.seconds = static_cast<std::uint64_t>( wholeSeconds );
  periodTime0.field.nanoseconds = static_cast<std::uint64_t>( nanoSeconds );
  periodTime0.field.subNanoseconds = static_cast<std::uint64_t>( subNanoseconds );
//  logger->Always("PERIODTIME0: SEC=%d, NANO=%d, SUB_NANO=%d, high=0x%08X, low=0x%08X\n",
//                  periodTime0.field.seconds, periodTime0.field.nanoseconds, periodTime0.field.subNanoseconds, 
//                  periodTime0.reg.high, periodTime0.reg.low);

  // Configure trigger time with the supplied start time...
  wholeSeconds = static_cast<long double>(static_cast<std::int64_t>(startTime_s));
  subSeconds = (startTime_s - wholeSeconds);
  nanoSeconds = subSeconds * 1000000000.0;
  wholeNano = static_cast<long double>( static_cast<std::uint64_t>(nanoSeconds));
  subNanoseconds = (nanoSeconds - wholeNano) * 10000.0;

  trigTime0.word = 0;
  trigTime0.field.seconds = static_cast<std::uint64_t>( wholeSeconds );
  trigTime0.field.nanoseconds = static_cast<std::uint64_t>( nanoSeconds );
  trigTime0.field.stateChange = 0;
//  logger->Always("TRIGTIME0: SEC=%d, NANO=%d, STATECHANGE=%d, high=0x%08X, low=0x%08X\n",
//                  trigTime0.field.seconds, trigTime0.field.nanoseconds, trigTime0.field.stateChange, 
//                  trigTime0.reg.high, trigTime0.reg.low);

  // Set for square wave output. Period time registers are loaded with
  // 1/2 the desired output trigger period.
  syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::PERIODTIME0_L, 
                                      static_cast<std::uint32_t>(periodTime0.reg.low) );
  syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::PERIODTIME0_H,
                                      static_cast<std::uint32_t>(periodTime0.reg.high) );

  // Everything should be disabled at this point! 
  // Enable trigger0. 
  // Note: Since this example is using rising edge trigger(positive sense)
  //       we can just leave period0_set == 0;
  eventCtrl = 0;
  eventCtrl |= period0Sense;     // set == 0, init == 1
  eventCtrl |= 0x00000004l;      // enable trigger0
  syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::EVENTCTRL, eventCtrl );
  
  // Now write the trigger time
  syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::TRIGTIME0_L, 
                                      static_cast<std::uint32_t>(trigTime0.reg.low) );
  syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::TRIGTIME0_H,
                                      static_cast<std::uint32_t>(trigTime0.reg.high) );

  // Turn everything back on
  eventCtrl = ( 0x00000001l   |        // Enable event0 (Camera trigger out)
                0x00000004l   |        // Enable trigger0
                0x00000010l   |        // Enable period0
                0x00000040l   |        // Enable period0 output
                period0Sense  |        // set == 0, init == 1
                0x00018000l );         // Start period0 on trigger0
//  logger->Always("ENABLE: eventCtrl=0x%08X\n", eventCtrl);
  std::cout << "ENABLE: eventCtrl=0x"
            << std::hex << std::setfill('0') << std::setw(8)<< eventCtrl << "\n";

  syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::EVENTCTRL, eventCtrl);

}
// End of enableSquareWavePeriodicOut()


//--------------------- enableProgDutyCyclePeriodicOut() -----------------------
//
void enableProgDutyCyclePeriodicOut( 
  long double periodIn_s, long double startTime_s, long double expTime_s)
{
  uint32_t eventCtrl; 
  PeriodTime periodTime0;    // Maps PERIODTIMEn_H/L parts to register bits
  PeriodTime periodTime1;    // Maps PERIODTIMEn_H/L parts to register bits
  TrigTime   trigTime0;      // Maps TRIGTIMEn_H/L parts to register bits

  // Configure period0 time with the exposure time...
  long double wholeSeconds = static_cast<long double>(static_cast<std::int64_t>(expTime_s));
  long double subSeconds = (expTime_s - wholeSeconds);
  long double nanoSeconds = subSeconds * 1000000000.0;
  long double wholeNano = static_cast<long double>( static_cast<std::uint64_t>(nanoSeconds));
  long double subNanoseconds = (nanoSeconds - wholeNano) * 10000.0;
  
  periodTime0.word = 0;
  periodTime0.field.seconds = static_cast<std::uint64_t>( wholeSeconds );
  periodTime0.field.nanoseconds = static_cast<std::uint64_t>( nanoSeconds );
  periodTime0.field.subNanoseconds = static_cast<std::uint64_t>( subNanoseconds );
//  logger->Always("PERIODTIME0: SEC=%d, NANO=%d, SUB_NANO=%d, high=0x%08X, low=0x%08X\n",
//                  periodTime0.field.seconds, periodTime0.field.nanoseconds, periodTime0.field.subNanoseconds,
//                  periodTime0.reg.high, periodTime0.reg.low);
  std::cout << std::setfill('0') << std::setw(8)
            << "PERIODTIME0: SEC=" << std::dec << periodTime0.field.seconds
            << ", NANO=" << std::dec << periodTime0.field.nanoseconds
            << ", SUB_NANO=" << std::dec << periodTime0.field.subNanoseconds
            << ", high=0x" << std::hex << periodTime0.reg.high
            << ", low=0x" << std::hex << periodTime0.reg.low << "\n";

  // Configure period1 time with difference of (periodIn - expTime)...
  long double remTime = periodIn_s - expTime_s;
  wholeSeconds = static_cast<long double>(static_cast<std::int64_t>(remTime));
  subSeconds = (remTime - wholeSeconds);
  nanoSeconds = subSeconds * 1000000000.0;
  wholeNano = static_cast<long double>( static_cast<std::uint64_t>(nanoSeconds));
  subNanoseconds = (nanoSeconds - wholeNano) * 10000.0;
  
  periodTime1.word = 0;
  periodTime1.field.seconds = static_cast<std::uint64_t>( wholeSeconds );
  periodTime1.field.nanoseconds = static_cast<std::uint64_t>( nanoSeconds );
  periodTime1.field.subNanoseconds = static_cast<std::uint64_t>( subNanoseconds );
//  logger->Always("PERIODTIME1: SEC=%d, NANO=%d, SUB_NANO=%d, high=0x%08X, low=0x%08X\n",
//                  periodTime1.field.seconds, periodTime1.field.nanoseconds, periodTime1.field.subNanoseconds,
//                  periodTime1.reg.high, periodTime1.reg.low);
  std::cout << std::setfill('0') << std::setw(8)
            << "PERIODTIME1: SEC=" << std::dec << periodTime1.field.seconds
            << ", NANO=" << std::dec << periodTime1.field.nanoseconds
            << ", SUB_NANO=" << std::dec << periodTime1.field.subNanoseconds
            << ", high=0x" << std::hex << periodTime1.reg.high
            << ", low=0x" << std::hex << periodTime1.reg.low << "\n";

  // Configure trigger time with the supplied start time...
  wholeSeconds = static_cast<long double>(static_cast<std::int64_t>(startTime_s));
  subSeconds = (startTime_s - wholeSeconds);
  nanoSeconds = subSeconds * 1000000000.0;
  wholeNano = static_cast<long double>( static_cast<std::uint64_t>(nanoSeconds));
  subNanoseconds = (nanoSeconds - wholeNano) * 10000.0;

  trigTime0.word = 0;
  trigTime0.field.seconds = static_cast<std::uint64_t>( wholeSeconds );
  trigTime0.field.nanoseconds = static_cast<std::uint64_t>( nanoSeconds );
  trigTime0.field.stateChange = 0;
//  logger->Always("TRIGTIME0: SEC=%d, NANO=%d, STATECHANGE=%d, high=0x%08X, low=0x%08X\n",
//                  trigTime0.field.seconds, trigTime0.field.nanoseconds, trigTime0.field.stateChange, 
//                  trigTime0.reg.high, trigTime0.reg.low);

  // Set for programmable duty cycle output. Period time registers are loaded with
  // exposure time and (period-exposureTime) values.
  syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::PERIODTIME0_L, 
                                      static_cast<std::uint32_t>(periodTime0.reg.low) );
  syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::PERIODTIME0_H,
                                      static_cast<std::uint32_t>(periodTime0.reg.high) );

  syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::PERIODTIME1_L, 
                                      static_cast<std::uint32_t>(periodTime1.reg.low) );
  syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::PERIODTIME1_H,
                                      static_cast<std::uint32_t>(periodTime1.reg.high) );

  // Everything should be disabled at this point! 
  // Enable trigger0. 
  // Note: Since this example is using rising edge trigger(positive sense)
  //       we can just leave period0_set == 0;
  eventCtrl = 0;
  eventCtrl |= period0Sense;     // set == 0, init == 1
  eventCtrl |= 0x00000004l;      // enable trigger0
  syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::EVENTCTRL, eventCtrl );
  
  // Now write the trigger time
  syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::TRIGTIME0_L, 
                                      static_cast<std::uint32_t>(trigTime0.reg.low) );
  syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::TRIGTIME0_H,
                                      static_cast<std::uint32_t>(trigTime0.reg.high) );

  // Turn everything back on
  eventCtrl = ( 0x00000001l   |        // Enable event0 (Camera trigger out)
                0x00000004l   |        // Enable trigger0
                0x00000010l   |        // Enable period0
                0x00000040l   |        // Enable period0 output
                period0Sense  |        // set == 0, init == 1
                0x00002000l   |        // Enable programmable duty cycle
                0x00018000l );         // Start period0 on trigger0
//  logger->Always("ENABLE: eventCtrl=0x%08X\n", eventCtrl);
  syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::EVENTCTRL, eventCtrl);

}
// End of enableProgDutyCyclePeriodicOut()


// END UTILITY METHODS
//==============================================================================


int main (int argc, char **argv) {
  terminateEventMonitor = false;                // Used by EventMonitor::doRun()

  // Instantiate Logger with default settings
  // PTP_ConfigLog cfglog;
  // cfglog.logLevel = LOG_LVL;
  logger = ptp::Logging::getRootLogger();
  logger->setLevel( ptp::log::Level::debug );

  // banner
  logger->Always(STD_BANNER);

  // create syn1588 object
  syn1588.reset( new Syn1588( CARD_NUM ) );

  if (!syn1588->present()) {
//    logger->Error("Error: Could not find syn1588(R) Hardware\n");
    std::cout << "Error: Could not find syn1588(R) Hardware\n";
    return -1;
  }

//  logger->Always("First, disable all events, triggers and periods.\n");
  std::cout << "First, disable all events, triggers and periods.\n";

  // clear event control register 2x (as recommended in docs)
  syn1588->getSyn1588Ifc()->writeReg(Syn1588Ifc::EVENTCTRL, 0);
  syn1588->getSyn1588Ifc()->writeReg(Syn1588Ifc::EVENTCTRL, 0);

  // Initialize PERIOD0 output for currently selected trigger sense.
  syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::EVENTCTRL, period0Sense );
 
  // Set the IOMATRIX register for the desired configuration(see above).
  syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::IOMATRIX, iomTriggering );

  // Start up the event monitor thread
  std::thread eventMonitorThread( eventMonitor ); 

  // Clear interrupts
  syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::IRSRC, ~0x0C00 );

  //  - Enable period0 to start at trigtime0.
  //  - wait for a desired start time, in this example an integral second.
  //  - wait for the event to occur which should be coincident with the trigger output.
  //  - delay for some period time which simulates an exposure time.
  //  - At this point the rate/desired trigger time for the next exposure may change
  //    so disable the event interrupt and periodic output
  //  - Repeat. For this example we just stick with enable, trigger, disable every 1 second.
  //
  // Expected results:
  // I would expect that without the need to perform any wait, I would see an event timestamp
  // and output trigger pulse every second on the second.
  // Instead, without any wait and with a disable/enable when an event occurs, which should be
  // at every (sec + 20ms) mark, I wind up seeing a positive edge transition and event0 
  // timestamp every 500ms. 
  // If I wait until the amount of time programmed in PERIOD0 has passed before (re)enabling
  // then I get the output I would expect with this demo, an output pulse and timestamp every 
  // second on the second. 
//  logger->Always("\n*********************** SQUARE WAVE OUTPUT **************************\n");
  std::cout << "\n*********************** SQUARE WAVE OUTPUT **************************\n";
  for( int i=0; i<30; i++ )
  {
    // To make things easy to read, start on next one-second boundary.
    Timestamp_t now {0,0,0};
    Timestamp_t start {0,0,0};
    syn1588->getTime(now);
    start.sec_l = now.sec_l + 1;
//    logger->Always("enableSquareWavePeriodicOut");
    std::cout << "enableSquareWavePeriodicOut\n";
    enableSquareWavePeriodicOut( PERIOD_TIME, start.sec_l );
//    enableProgDutyCyclePeriodicOut( PERIOD_TIME, start.sec_l, EXPOSURE_TIME );

    // Enable Event0 interrupt, Be sure we start with a clean FIFO
    std::uint32_t iren = syn1588->getSyn1588Ifc()->readReg( Syn1588Ifc::IREN );
    syn1588->clearEvent0Fifo();
    iren |= 0x00000400l;
    syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::IREN, iren );

    // Wait for "trigger". We expect it only occur once every second on the second.
    while( !event0occurred );  
    event0occurred = false;

    // Simulate exposure time
    delay( EXPOSURE_TIME );

    // Disable Event0 interrupt and periodic output
    // NOTE: With the way my disable is written, this will force PERIOD0 output
    //       to 0. On the o-scope it will look like a 20ms pulse but in fact it
    //       is a square wave that has been disabled before the time in PERIOD0
    //       has expired.
    syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::IREN, 0 );
	
    // =======================================================
    // LR: workaround
    disablePeriod();    // disable the current running period
    startLongPeriod();  // set new (very long) period as a workaround to avoid events until next trigger
    stopPeriod();       // stop the workaround-period: from this point on you can set a new period0 to start with a trigger
    //========================================================
	
    //disablePeriodicOut();

    // Uncomment the following line to get the expected behavior of an output
    // pulse and timestamp every second on the second.
    // This effectively puts the disable/re-enable in the second half of the
    // currently programmed period.
//    delay( (PERIOD_TIME/2.0) - EXPOSURE_TIME );  // TO MAKE SQUARE WAVE WORK
  }

  // Disable Event0 interrupt and periodic output
  syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::IREN, 0 );
  disablePeriodicOut();

  terminateEventMonitor = true;
  eventMonitorThread.join();

  // Reset the IOMATRIX register
  syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::IOMATRIX, 0 );
  syn1588->getSyn1588Ifc()->writeReg( Syn1588Ifc::IOMATRIX, iomReset );

  return 0;
}
