# CSF imports
import atst.base.util.Misc as BaseMisc
import atst.base.util.RemoteAction as RemoteAction
import atst.base.util.ra.NullAction as NullAction
import atst.cs.controller.ActionResponse as ActionResponse
import atst.cs.data.AttributeTable as AttributeTable
import atst.cs.services.App as App
import atst.cs.services.AppDB as AppDB
import atst.cs.services.event.EventCallbackAdapter as EventCallbackAdapter
import atst.cs.test.TestConfiguration as TestConfiguration
import atst.cs.util.ShellCommand as ShellCommand

# CSS imports
import atst.css.util.CameraId as CameraId
import atst.css.util.global.Global as Global
import atst.css.util.global.parts.CamMode as CamMode
import atst.css.util.global.parts.StartMode as StartMode
import atst.css.util.global.parts.TriggerReference as TriggerReference

# Python imports
import argparse
import sys
import threading
import time
from time import sleep
from datetime import datetime, timedelta

DEFAULT_FIXED_PREFIX = 'css.tools'
DEFAULT_CC_ID = 'cc_default'
ACQUIRING_WAIT_TIME = 10.0  # Seconds to wait for DAT to transition to acquiring
                            # when it's started. 10s allows for NIRSP DATs to
                            # start and transition from WAITING TO ACQUIRING.

# Custom action that can be used by the argument parser to check whether a
# supplied value is >= 0.
# Note that geq 0 is equivalent to using HasLimitsAction where
# minium=0, maximum=None. This is just clearer (I think).
class GEQZeroAction(argparse.Action):
    #---------------------------- __call__() -----------------------------------
    #
    def __call__(self, parser, namespace, value, option_string=None):
        if not value >= 0:
            raise argparse.ArgumentError(self, "time value must be >= 0")
        setattr(namespace, self.dest, value)
    #
    # End of GEQZeroAction.__call__()


# Custom argparse action that is used to verify that the positional argument
# 'dluName' is indeed a valid DLU name that is contained in the App DB.
class ValidDLUAction(argparse.Action):
    #---------------------------- __call__() -----------------------------------
    #
    def __call__(self, parser, namespace, value, option_string=None):
        if not AppDB.getDLU(value):
            raise argparse.ArgumentError(self,
                '\'' + value + '\' is not a valid DLU name!')
        setattr(namespace, self.dest, value)
    #
    # End of ValidDLUAction.__call__()


# Setup parser for command-line arguments
cmd_line_parser = argparse.ArgumentParser(add_help=True)

cmd_line_parser.add_argument('--showDescription', action='store_true',
    help='display a description of this test and exit.')

cmd_line_parser.add_argument('--datFile',
    action='store', dest='datFile', default=None,
    help='name of file containing DAT as obtained using CssCcEbReader')

cmd_line_parser.add_argument("--datId0",
    action="store", dest="datId0",
    help='Root node ID0 of DAT contianed \'--datFile\'. If omitted, '\
         'the EB ID containing \'root\' in its name will be used as ID0.')

cmd_line_parser.add_argument("--source",
    default=DEFAULT_FIXED_PREFIX, dest="source",
    help="FQ Prefix of source controller used to generate file named with --datFile.")

cmd_line_parser.add_argument('--execCount', type=int,
    action=GEQZeroAction, dest="execCount", default=3,
    help='minimum number of times to execute the DAT before shutting down')

cmd_line_parser.add_argument('--numTestCycles', type=int,
    action=GEQZeroAction, dest="numTestCycles", default=1,
    help='total number of times to execute this test')

cmd_line_parser.add_argument('--debug', action='store_true',
    help='enable debug output')

cmd_line_parser.add_argument('--dryRun', action='store_true',
    help='execute without submitting configurations to CSS')

cmd_line_parser.add_argument('--verbose', action='store_true',
    help='increase output verbosity')

# Positional arguments
cmd_line_parser.add_argument('dluName', type=str,
    action=ValidDLUAction, help='name of CSS DLU')


class DatStatusCallbackHandler( EventCallbackAdapter ):
    def __init__(self, cmd_line_args,
        ev_acquiring,       # Threading event to signal DAT is acquiring (running)
        ev_target_reached   # Threading event to signal exec_count has been reached
        ):

        EventCallbackAdapter.__init__(self)
        self._cla = cmd_line_args              # copy command line arguments
        self._ev_acquiring = ev_acquiring
        self._ev_target_reached = ev_target_reached

        self._dat_acqStatus = None             # Current DAT acquisition status

        # NOTE: If jython ever goes to Python 3 this can be an enum!
        self.WAIT_FOR_WAITING   = 1
        self.WAIT_FOR_ACQUIRING = 2
        self.WAIT_FOR_TARGET_COUNT = 3
        self.WAIT_FOR_SHUTDOWN = 4
        self._callback_state = self.WAIT_FOR_WAITING
    #
    # End of DatStatusCallbackHandler.__init__()

    # NOTE: If jython ever goes to Python 3 this can be replaced with state.name()/.value()
    def state2string(self, state):
        if state is self.WAIT_FOR_WAITING:
            string = 'WAIT_FOR_WAITING ({})'.format(self.WAIT_FOR_WAITING)
        elif state is self.WAIT_FOR_ACQUIRING:
            string = 'WAIT_FOR_ACQUIRING ({})'.format(self.WAIT_FOR_ACQUIRING)
        elif state is self.WAIT_FOR_TARGET_COUNT:
            string = 'WAIT_FOR_TARGET_COUNT ({})'.format(self.WAIT_FOR_TARGET_COUNT)
        else:
            string = 'WAIT_FOR_SHUTDOWN ({})'.format(self.WAIT_FOR_SHUTDOWN)
        return string
    #
    # End of DatStatusCallbackHandler.__init__()


    def callback( self, mesgName ):
        # Retrieve the attribute table for this CSS event and then grab
        # a couple attributes we need:
        #   dat:acqStatus - Tells us the acquisition status which is one of
        #                   'preview', 'waiting', 'acquiring' which are the
        #                   only value we care about in this code.
        #   dataTier:execCountCurr - Gives us the current DAT execution count

        at = self.getValue()
        acqStatus = at.getString( 'dat:acqStatus' )
        execCountCurr = at.getIntegerArray('dataTier:execCountCurr')[0]

        if self._cla.debug:
            print('callback_state={: <25}; acqStatus={: <9}; execCountCurr={}'.format(
                self.state2string(self._callback_state),
                acqStatus, execCountCurr))
            # print('callback_state: {}'.format(self.state2string(self._callback_state)))
            # print('acqStatus     : {}'.format(acqStatus))
            # print('execCountCurr : {}'.format(execCountCurr))
            if self._cla.verbose:
              print('datStatus event table:\n{}'.format(at))

        if 'preview' == acqStatus:
            return                              # Ignore preview mode

        # Simple state machine based on acquisition status...
        if self.WAIT_FOR_WAITING == self._callback_state:
            if 'waiting' == acqStatus:
                # Nothing to do for WAITING state, now wait for ACQURING
              self._callback_state = self.WAIT_FOR_ACQUIRING

        elif self.WAIT_FOR_ACQUIRING == self._callback_state:
            if 'acquiring' == acqStatus:

                if self._cla.debug:
                    print('DAT has been started and is acquiring (running). '\
                          'Notifying listeners...')

                # Signal via threading event that the DAT is 'acquiring'...
                self._ev_acquiring.set()

                # Now we wait for the desired execution count...
                self._callback_state = self.WAIT_FOR_TARGET_COUNT

        elif self.WAIT_FOR_TARGET_COUNT == self._callback_state:
            if execCountCurr >= self._cla.execCount:

                if self._cla.debug:
                    print('Target execution count of {} reached. '\
                          'Notifying listeners...'.format(execCountCurr))

                # Signal via threading event that the target execCount has been
                # reached...
                self._ev_target_reached.set()

                # All done. Nothing to do now but wait to terminate...
                self._callback_state = self.WAIT_FOR_SHUTDOWN

    #
    # End of DatStatusCallbackHandler.callback()


class StartDAT(threading.Thread):
    #---------------------------- __init__() -----------------------------------
    def __init__(self, cmd_line_args, mgmt_ctrl_name, cfg_to_start):
        threading.Thread.__init__(self)
        self._cla = cmd_line_args
        self._mgmt_ctrl_name = mgmt_ctrl_name
        self._cfg_to_start = cfg_to_start

        # Connect to the management controller...
        self._vcc = App.connect(self._mgmt_ctrl_name)

    #
    # End of StartDAT.__init__()


    #------------------------------- run() -------------------------------------
    # threading.Thread.run() overload
    #
    def run(self):
        success = self.do_submitAndWait(
            self._cfg_to_start,
            'Starting \'{}\' on \'{}\'...'.format(
                self._cfg_to_start.getString(self._mgmt_ctrl_name + Global.ID0),
                self._mgmt_ctrl_name))

    #
    # End of StartDAT.run()


    # Submits a configuration to the VCC and waits for an action response.
    #
    # config - The config to be submitted
    # what - A short descriptive string of what is being submitted
    def do_submitAndWait(self, config, what):
        success = True

        if self._cla.verbose:
            print(what)

        if self._cla.debug:
            print('Submitting configuration: {}\n'.format(config))

        if not self._cla.dryRun:
            ra = BaseMisc.submitAndWait(NullAction(), self._vcc, config)
            ar = ra.getActionResponse()
            if not ActionResponse.isAborted(ar):
                print('Failure {} - {}'.format(what, ar.errString))
                success = False

        return success
    #
    # End of StartDAT.do_submitAndWait()


class Test_ShutdownWhileRunning:
    #---------------------------- __init__() -----------------------------------
    #
    def __init__(self, cmd_line_args):
        self._cla = cmd_line_args

        self._vcc = None              # Will connect after CSS is started
        self._datId0 = None
        self._num_errors = 0

        self._shell_cmd_running = ShellCommand(
            '$ATST/bin/DLUManager --dlu={} --status | grep -i -q running'.format(
                self._cla.dluName) )

        self._shell_cmd_unloaded = ShellCommand(
            '$ATST/bin/DLUManager --dlu={} --status | grep -i -q unloaded'.format(
                self._cla.dluName) )

        self._shell_cmd_cssUp = ShellCommand(
            '$ATST/bin/DLUManager --dlu={} --start'.format(
                self._cla.dluName) )

        self._shell_cmd_cssDown = ShellCommand(
            '$ATST/bin/DLUManager --dlu={} --stop'.format(
                self._cla.dluName) )

        # Using the DLU for this CSS instance get the mgmt controllers name...
        self._mgmt_ctrl_name = AppDB.getDLU(
            self._cla.dluName).getComponents()[0]

        # Python threading events, to be used by the datStatus event handler,
        # for signaling DAT is 'acquiring' and when we've reached the target
        # 'execCount'.
        self._ev_acquiring = threading.Event()
        self._ev_acquiring.clear()
        self._ev_target_reached = threading.Event()
        self._ev_target_reached.clear()

        # Prep for subscribing to the CSS Camera Controller DAT Status event...
        self._cam_ctrl_name = self._mgmt_ctrl_name + '.cam'
        self._dat_status_event_name = self._cam_ctrl_name + '.datStatus'
        self._dat_status_callback = DatStatusCallbackHandler(
            self._cla,
            self._ev_acquiring, self._ev_target_reached)

        # Create a daily referenceT0
        self._referenceT0_str      = time.strftime("%Y-%m-%dT00:00:00Z",time.gmtime())
        self._referenceT0_atstdate = AtstDate.parse(self._referenceT0_str)
        self._referenceT0_ns       = self._referenceT0_atstdate.getNanoTime()
        self._referenceT0_sec      = self._referenceT0_ns / 1000000000.0

        # Instantiate test configurations and initialize as much as possible...
        self._cfg_dat_to_run = TestConfiguration()
        self._cfg_dat_to_run.setFQPrefix(self._mgmt_ctrl_name)
        self._cfg_dat_to_run.insert(Global.CAM_MODE, CamMode.startWithoutSave.name())
        self._cfg_dat_to_run.insert(Global.START_MODE, StartMode.immediate.name())
        self._cfg_dat_to_run.insert(Global.EXEC_COUNT0, 0)
        self._cfg_dat_to_run.insert(Global.REFERENCE_T0, self._referenceT0_str)

    #
    # End of TestShutdownWhileRunning.__init__()


    def cssIsUnloaded(self):
        if self._cla.verbose:
            sys.stdout.write('Verifying CSS is unloaded...')
            sys.stdout.flush()

        x = self._shell_cmd_unloaded.execute()
        if x == 0:
            if self._cla.verbose: print('OK!')
            return True

        return False

    def cssIsRunning(self):
        if self._cla.verbose:
            sys.stdout.write('Verifying CSS is running...')
            sys.stdout.flush()

        x = self._shell_cmd_running.execute()
        if x == 0:
            if self._cla.verbose: print('OK!')
            return True

        return False

    def startCss(self):
        if self._cla.verbose:
            sys.stdout.write('Starting CSS...')
            sys.stdout.flush()

        x = self._shell_cmd_cssUp.execute()
        if x == 0:
            if self._cla.verbose: print('OK!')
            return True

        return False

    def stopCss(self):
        if self._cla.verbose:
            sys.stdout.write('Stopping CSS...')
            sys.stdout.flush()

        x = self._shell_cmd_cssDown.execute()
        if x == 0:
            if self._cla.verbose: print('OK!')
            return True
        return False

    def execute(self):
        global ACQUIRING_WAIT_TIME
        success = True
        failure = False
        status = success

        try:
            assert self.cssIsUnloaded(), 'ERROR - CSS is not unloaded!'

            assert self.startCss(), 'ERROR - CSS did not start!'

            assert self.cssIsRunning(), 'ERROR - CSS is not running!'

            self.connectToVcc()

            self.setupCssForTest()

            id0Duration = self.getId0Duration()

            # Subscribe to the datStatus event. The event handler will
            # notify via threading events when the DAT is 'acquiring'
            # and then when we've reached the targer 'execCount'.
            Event.subscribe(
                self._dat_status_event_name,
                self._dat_status_callback)

            # Spawn thread to submit config to start either the default
            # camera configuration or a supplied DAT that was loaded from file.
            datThread = StartDAT(
                self._cla,             # For any cmdLine flags needed
                self._mgmt_ctrl_name,  # For App.connect
                self._cfg_dat_to_run   # The configuration to execute
                )
            datThread.start()
            while not datThread.isAlive(): time.sleep(1)
            time.sleep(1)

            # Wait for the datStatus event callback to signal that
            # the DAT has started and is reporting 'acquiring'.
            if self._cla.verbose:
                sys.stdout.write('Waiting for DAT to start acquiring...')
                sys.stdout.flush()

#            dat_is_acquiring = self._ev_acquiring.wait(id0Duration)
            dat_is_acquiring = self._ev_acquiring.wait(ACQUIRING_WAIT_TIME)

            assert dat_is_acquiring, 'ERROR: Timeout waiting for DAT to start!'

            if self._cla.verbose:
                print('OK!')

            # Wait for the datStatus event callback to signal that
            # the execCount has reached the target count.
            if self._cla.verbose:
                sys.stdout.write('Waiting for target execution count notification...')
                sys.stdout.flush()

            # TODO: get exec time and calc wait time and use
            timeout = id0Duration * (self._cla.execCount + 1)
            target_reached = self._ev_target_reached.wait(timeout)

            assert target_reached, 'ERROR: Timeout waiting for target execution count!'

            if self._cla.verbose:
                print('OK!')

            if self._cla.verbose:
                print('=======================================================')
                print('Target execution count reached, shutting down CSS....')
                print('=======================================================')


            assert self.stopCss(), 'ERROR - CSS did not stop!'

            assert self.cssIsUnloaded(), 'ERROR - CSS is not unloaded!'
            success=True

        except AssertionError as ae:
            print(ae)
            success = False

        return success
    #
    # End of TestShutdownWhileRunning.execute()


    def connectToVcc(self):
        self._vcc = App.connect(self._mgmt_ctrl_name)

        assert not self._vcc == None,\
            'ERROR: Cannot connect to {}! Is it running?'.format(
                self._mgmt_ctrl_name)
    #
    # End of TestShutdownWhileRunning.connectToVcc()


    def loadDAT(self):
        print('Loading datFile \'' + self._cla.datFile + '\'...')

        cmd = ('ajava atst.css.tools.CssCcEbTools\\$SubmitDirect' +
              ' --vcName=' + self._mgmt_ctrl_name +
              ' --fileName=' + self._cla.datFile +
              ' --fqPrefix=' + self._cla.source)

        if self._cla.debug: print(cmd)

        # Execute the shell command and assert an error if it fails...
        shellCmd = ShellCommand(cmd)
        assert shellCmd.execute() == 0, shellCmd.getStderr()

        # Get list of IDs now loaded
        at = AttributeTable()
        at.insert(Global.ID_LIST)
        at = at.requalify(self._mgmt_ctrl_name)
        at = self._vcc.get(at)
        at = at.requalify(self._mgmt_ctrl_name)

        listOfIds = at.getStringArray(Global.ID_LIST)
        if self._cla.debug: print('List of currently loaded IDs:')
        if self._cla.datId0 is None:
            # Find id containing 'root' in its name. This will be ID0.
            for id in listOfIds:
                if self._cla.debug: print('\t{}'.format(id))
                if id.find('root') >= 0:
                    self._datId0 = id
        else:
            # Verify named datId0 is actually in the list of loaded IDs.
            for id in listOfIds:
                if self._cla.debug: print('\t{}.format(id)')
                if id.find(self._cla.datId0) >= 0:
                    self._datId0 = id

        if self._datId0 is None:
            assert not self._cla.datId0 is None, \
                'ERROR: Cannot find an ExecBlock ID containing \'root\' in its name.'

            assert False, \
                 'ERROR: Cannot find datId0=\'{}\' in list of loaded IDs.'.format(
                    self._cla.datId0)
    #
    # End of TestShutdownWhileRunning.loadDAT()


    # Used immediately after connecting to a running VCC to determine the
    # current value of .global:ID0 and it's duration.
    def setupCssForTest(self):
        if self._cla.datFile is None:
            # We're going to test using the default camera configuration.
            # Query the CSS for the current value of .global:ID0 which will be the
            # default CC ID then make the configuration to run it.
            at = AttributeTable()
            at.setFQPrefix(self._mgmt_ctrl_name)
            at.insert(Global.ID0)
            at.insert(Global.CAMERA_ID)
            at.insert(Global.TRIGGER_REFERENCE)
            at = at.requalify(self._mgmt_ctrl_name)
            at = self._vcc.get(at)
            at = at.requalify(self._mgmt_ctrl_name)

            if self._cla.debug:
                print('Default Globals AT: {}\n'.format(at))

            self._camera_id = CameraId.create(at.getStringArray(Global.CAMERA_ID))
            self._datId0 = at.getString(Global.ID0)

            print('Camera: {}'.format(self._camera_id))

            # Add ID0 and triggeReference to the config that will be
            # submitted to the CSS to start the DAT...
            self._cfg_dat_to_run.insert(Global.ID0, self._datId0)
            self._cfg_dat_to_run.insert(Global.TRIGGER_REFERENCE,
                at.getString(Global.TRIGGER_REFERENCE))

        else:
            # We're going to test using the DAT file from the cmd line.
            # Get it loaded into the CSS and running in preview mode...
            self.loadDAT()

            # Get list of IDs now loaded
            at = AttributeTable()
            at.insert(Global.CAMERA_ID)
            at.insert(Global.TRIGGER_REFERENCE)
            at = at.requalify(self._mgmt_ctrl_name)
            at = self._vcc.get(at)
            at = at.requalify(self._mgmt_ctrl_name)

            # Add ID0 and triggeReference to the config that will be
            # submitted to the CSS to start the DAT...
            self._cfg_dat_to_run.insert(Global.ID0, self._datId0)
            self._cfg_dat_to_run.insert(Global.TRIGGER_REFERENCE,
                at.getString(Global.TRIGGER_REFERENCE))

            # Start the DAT from the file in preview mode...
            self.previewDatFromFile()

        if self._cla.debug:
            self._cfg_dat_to_run.show('Config for test')
    #
    # End of TestShutdownWhileRunning.setupCssForTest()


    def previewDatFromFile(self):
        cfg = TestConfiguration()
        cfg.setFQPrefix(self._mgmt_ctrl_name)
        cfg.insert(Global.ID0, self._datId0)
        cfg.insert(Global.CAM_MODE, CamMode.preview.name())
        cfg.insert(Global.START_MODE, StartMode.immediate.name())
        cfg.insert(Global.TRIGGER_REFERENCE, TriggerReference.absolute.name())
        cfg.insert(Global.EXEC_COUNT0, 0)

        self.do_submitAndWait(
            cfg, 'Starting \'{}\' in preview mode on \'{}\'...'.format(
                cfg.getString(self._mgmt_ctrl_name + Global.ID0),
                self._mgmt_ctrl_name))
    #
    # End of TestShutdownWhileRunning.previewDatFromFile()


    # Submits a configuration to the VCC and waits for an action response.
    #
    # config - The config to be submitted
    # what - A short descriptive string of what is being submitted
    def do_submitAndWait(self, config, what):
        print(what)

        if self._cla.verbose:
            print('Submitting configuration: {}\n'.format(config))

        if not self._cla.dryRun:
            ra = BaseMisc.submitAndWait(NullAction(), self._vcc, config)
            ar = ra.getActionResponse()
            assert ActionResponse.isOK(ar), \
                'Failure {} - {}'.format(what, ar.errString)
    #
    # End of StartDAT.do_submitAndWait()


    def getId0Duration(self):
        at = AttributeTable()
        at.setFQPrefix(self._mgmt_ctrl_name)
        at.insert(Global.ID0_DURATION)
        at = at.requalify(self._mgmt_ctrl_name)
        at = self._vcc.get(at)
        at = at.requalify(self._mgmt_ctrl_name)
        if self._cla.debug:
            print('   DAT Duration: {}\n'.format(at.getDouble(Global.ID0_DURATION)))

        return at.getDouble(Global.ID0_DURATION)
    #
    # End of TestShutdownWhileRunning.getId0Duration()


def main():
    global cmd_line_parser
    fail_count = 0

    cmd_line_args = cmd_line_parser.parse_args()

    if cmd_line_args.showDescription:
        print(
            'Driving JIRA: HLSOPS-3930\n'\
            'Purpose: Assure that the CSS can be brought down with a running DAT.\n'
            'Actions taken:\n'\
            '  - Uses the DLU Manager to verify the CSS is unloaded.\n'\
            '  - Uses the DLU Manager to start the CSS.\n'\
            '  - Using either the default camera configuration or DAT from a supplied\n'\
            '    DAT file, starts the DAT running using \'startWithoutSave.\'\n'\
            '  - Lets the DAT run for a specified minumum number of execution counts.\n'\
            '  - Shuts the CSS down.\n'\
            '  - Verifies the active configuration (DAT) terminates with ABORTED.')
        return

    test = Test_ShutdownWhileRunning(cmd_line_args)

    print('{: >6}: {}'.format(
        'Date', datetime.utcnow().strftime('%d/%m/%Y %H:%M:%S')))
    startTime = time.time()

    for cycle in range(1, cmd_line_args.numTestCycles+1):

        print('Test cycle #{:2d} of {} running...'.format(
            cycle, cmd_line_args.numTestCycles))

        success = test.execute()
        if not success: fail_count = fail_count + 1

        stsStr = 'PASS'
        if not success: stsStr = 'FAIL'

        print('Test cycle #{:2d} of {} complete - {}'.format(
            cycle, cmd_line_args.numTestCycles, stsStr))

    if fail_count > 0:
        print('Overall test status: FAIL\n')
        sys.exit(1)
    else:
        print('Overall test status: PASS\n')
        sys.exit(3)


if __name__ == '__main__':
#    clear_execution_time_data()
    main()
#    print_execution_time()
