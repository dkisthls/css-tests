import atst.base.util.Misc as BaseMisc
import atst.base.util.RemoteAction as RemoteAction
import atst.base.util.ra.NullAction as NullAction
import atst.base.util.ra.RemoteActionState as RemoteActionState
import atst.cs.controller.ActionResponse as ActionResponse
import atst.cs.data.AttributeTable as AttributeTable
import atst.cs.data.HealthReport as HealthReport
import atst.cs.util.AtstDate as AtstDate
import atst.cs.util.json.JsonUtil as JsonUtil
import atst.cs.util.Misc as Misc
import atst.cs.util.ShellCommand as ShellCommand
import atst.cs.data.Configuration as Configuration
import atst.cs.services.AppDB as AppDB
import atst.cs.services.event.EventCallbackAdapter as EventCallbackAdapter
import atst.cs.services.Health as Health
import atst.cs.services.health.ReportingHealthServiceTool as ReportingHealthServiceTool
import atst.cs.test.TestConfiguration as TestConfiguration
import atst.css.util.CameraId as CameraId
import atst.css.util.CameraIdSets as CameraIdSets
import atst.css.util.VCAttributeSets as VCAttributeSets
import atst.css.util.csh.parts.Sampling as Sampling
import atst.css.util.calculators.TriggerCalculator as TriggerCalculator
import atst.css.util.camconfig.CamConfig as CamConfig
import atst.css.util.camconfig.CamConfigFactory as CamConfigFactory
import atst.css.util.camconfig.parts.Calculated as Calculated
import atst.css.util.camconfig.parts.CamConfigId as CamConfigId
import atst.css.util.camconfig.parts.XYPair as XYPair
import atst.css.util.execblock.ExecBlock as ExecBlock
import atst.css.util.execblock.parts.CcNoOp as CcNoOp
import atst.css.util.execblock.parts.Exec as Exec
import atst.css.util.execblock.parts.ExecBlockId as ExecBlockId
import atst.css.util.execblock.parts.MetaData as MetaData
import atst.css.util.execblock.RootNode as RootNode
import atst.css.util.global.Global as Global
import atst.css.util.global.parts.CamMode as CamMode
import atst.css.util.global.parts.StartMode as StartMode
import atst.css.util.global.parts.TriggerReference as TriggerReference
from time import sleep
import argparse
import codecs     # For printing utf-8 DAT tree representation
import copy
import logging
import math
import random
import subprocess
import sys 
import threading
import time
import traceback

from jsonArgFileProcessor import JsonArgFileProcessor

from datetime import datetime, timedelta
from time import gmtime  # Use UTC rather than local for logging data/time

from java.lang import Exception as JavaException

EXIT_CODE_SUCCESS = 0               # Return exit code, no errors
EXIT_CODE_FAILURE = 1               # Return exit code, error occurred

MIN_ABORTCANCEL_WAIT_TIME = 0.01    # seconds
MIN_GENERIC_WAIT_TIME = 0.2         # For general delays and waits in previes
MAX_GENERIC_WAIT_TIME = 5.0         # For general delays and waits in preview
WAIT_ABORTCANCEL = 'abortCancel'    # Wait for random time based on DAT duration
WAIT_GENERIC = 'generic'            # Wait for random time based on fixed range

DEFAULT_NUM_PASSES = 100
DEFAULT_DURATION = 10.0
DEFAULT_FIXED_PREFIX = 'css.tools'

#---------- TEST MODES
#
MODE_PREVIEW = 'preview'        # ping-pong between DAT/default in preview only
MODE_ABORT = 'abort'            # Start DAT and issue abort before DAT completes
MODE_CANCEL = 'cancel'          # Start DAT and issue cancel before DAT completes
MODE_NO_ACTION = 'noAction'     # Start DAT and let it run to completion
all_test_modes = [MODE_PREVIEW, MODE_ABORT, MODE_CANCEL, MODE_NO_ACTION]

# For test summary string formatting, get the max test mode string length
MAX_WIDTH_TEST_MODE = 0
for m in all_test_modes:
    MAX_WIDTH_TEST_MODE = max(MAX_WIDTH_TEST_MODE, len(m))

ACTION_ABORT = 'abort'          # Abort DAT after some period of time
ACTION_CANCEL = 'cancel'        # Cancel DAT after some period of time
ACTION_NONE = 'noAction'        # Take no action
actions = [ACTION_ABORT, ACTION_CANCEL, ACTION_NONE]
#
#----

#---------- CAM MODES
#
# Action that can be sequence prior to starting the DAT
PRE_PREVIEW_CCDEFAULT = 'previewCcDefault'    # Execute default CC in preview
PRE_RESUBMIT_ID0 = 'resubmitId0'              # Resubmit all CC's/EB's of DAT
PRE_PREVIEW_ID0 = 'previewId0'                # Execute DAT in preview
PRE_DELAY = 'delay'                           # Add randome delay based on fixed range
preRunSequenceChoices=[PRE_PREVIEW_CCDEFAULT, PRE_RESUBMIT_ID0, PRE_PREVIEW_ID0, PRE_DELAY]

camModeChoices=[CamMode.startWithoutSave.name().encode('ascii'), \
                CamMode.startWithSave.name().encode('ascii'), \
                CamMode.startWithSaveEng.name().encode('ascii')]
DEFAULT_CAM_MODE=camModeChoices[0]
#
#----

#---------- START MODES
#
startModeChoices=[StartMode.atStartTime.name().encode('ascii'), \
                  StartMode.immediate.name().encode('ascii')]
DEFAULT_START_MODE=startModeChoices[1]
#
#----

# For test summary string formatting, get the max start mode string length
MAX_WIDTH_START_MODE = 0
for sm in startModeChoices:
    MAX_WIDTH_START_MODE = max(MAX_WIDTH_START_MODE, len(sm))

#---------- TRIGGER REFERENCE
#
triggerReferenceChoices=[TriggerReference.absolute.name().encode('ascii'), \
                         TriggerReference.timeSlice.name().encode('ascii')]
DEFAULT_TRIGGER_REFERENCE=triggerReferenceChoices[1]

# For test summary string formatting, get the max trigger reference string length
MAX_WIDTH_TRIGGER_REFERENCE = 0
for tr in triggerReferenceChoices:
    MAX_WIDTH_TRIGGER_REFERENCE = max(MAX_WIDTH_TRIGGER_REFERENCE, len(tr))
#
#----

DEFAULT_EXECCOUNT = 1

# Maximum allowed positive difference between scheduled and actual DAT
# completion time (in ms)
DEFAULT_COMP_TOLERANCE = 1.0

# For spacing test progression output on screen
SPACER_WIDTH = 15
SPACER = SPACER_WIDTH * ' '

# Custom argparse action that is used to verify that the positional argument
# 'dluName' is indeed a valid DLU name that is contained in the App DB.
class ValidDLUAction(argparse.Action):
    #---------------------------- __call__() -----------------------------------
    #
    def __call__(self, parser, namespace, value, option_string=None):
        if not AppDB.getDLU(value):
            raise argparse.ArgumentError(self,
                '\'' + value + '\' is not a valid DLU name!')
        setattr(namespace, self.dest, value)
    #
    # End of ValidDLUAction.__call__()


# Custom argparse action that is used to verify that a specified input file
# exists and is readable.
class FileExistsAction(argparse.Action):
    #---------------------------- __call__() -----------------------------------
    #
    def __call__(self, parser, namespace, value, option_string=None):
        try:
            with open(value, 'r') as f:
                setattr(namespace, self.dest, value)
        except IOError as ioe:
            raise argparse.ArgumentError(self, '{}'.format(ioe))
    #
    # End of ValidDLUAction.__call__()


# Setup parser for command-line arguments
cmdLineParser = argparse.ArgumentParser(add_help=True)

# --datFile and --testFile are mutually exclusive but one is required.
file_group = cmdLineParser.add_mutually_exclusive_group(required=True)
file_group.add_argument("--datFile",
    action=FileExistsAction, dest="datFile",
    help="name of file containing DAT as obtained using CssCcEbReader.")
file_group.add_argument("--testFile",
    action=FileExistsAction, dest="testFile",
    help="name of JSON test file containing test arguments.")

cmdLineParser.add_argument("--datDuration", type=float,
    default = DEFAULT_DURATION,
    action="store", dest="datDuration",
    help="duration, in seconds, of the DAT being tested.")

cmdLineParser.add_argument("--datId0",
    action="store", dest="datId0", 
    help="Root node ID0. If omitted, the EB ID containing \'root\' in its "\
         "name will be used as ID0.")

cmdLineParser.add_argument("--numPasses", type=int, 
    default=DEFAULT_NUM_PASSES,
    action="store", dest="numPasses",
    help="number of times to execute each test.")

cmdLineParser.add_argument("--modesToTest", choices=all_test_modes,
    default=all_test_modes,
    nargs='+', action="store", dest="modesToTest",
    help="one or more test modes to execute")

cmdLineParser.add_argument("--camMode", choices=camModeChoices,
    default=DEFAULT_CAM_MODE,
    action="store", dest="camMode",
    help="CSS cam mode when starting DAT")

cmdLineParser.add_argument("--startMode", choices=startModeChoices,
    default=DEFAULT_START_MODE,
    nargs='+', action="store", dest="startMode",
    help="CSS start mode for DAT")

cmdLineParser.add_argument("--startDelay", type=float,
    default=None,
    action="store", dest="startDelay",
    help='Defines the delay, in seconds, from "now" to when the DAT should start.')

cmdLineParser.add_argument("--execCount", type=int,
    default=DEFAULT_EXECCOUNT,
    action="store", dest="execCount",
    help="DAT execCount used only with mode=noAction.")

cmdLineParser.add_argument("--source", 
    default=DEFAULT_FIXED_PREFIX, dest="source",
    help="FQ Prefix of source controller used to generate file named with --datFile.")

cmdLineParser.add_argument("--triggerReference", 
    default=DEFAULT_TRIGGER_REFERENCE,
    nargs='+', action="store", dest="triggerReference",
    help="Value for global attribute {}".format(Global.TRIGGER_REFERENCE))

cmdLineParser.add_argument("--deleteAllIds",
    action="store_true",
    help="Clean all loaded IDs on completion")

cmdLineParser.add_argument("--fastAsPossible", 
    action="store_true",
    help="Run tests with no delays between DATs")

cmdLineParser.add_argument("--previewCcDefault", 
    action="store_true",
    help="Switch to cc_default after action completes")

cmdLineParser.add_argument("--preRunSequence", choices=preRunSequenceChoices,
    nargs='+', action="store", dest="preRunSequence",
    help="Sequence of actions prior to 'modeToTest'!")
    
cmdLineParser.add_argument("--verbose", help="increase output verbosity",
                    action="store_true")

cmdLineParser.add_argument("--compTolerance", type=float,
    default=DEFAULT_COMP_TOLERANCE,
    action="store", dest="compTolerance",
    help="Defines the maximum allowed positive difference, in ms, between scheduled and actual DAT completion time.")

# Positional arguments
cmdLineParser.add_argument('dluName', type=str,
    action=ValidDLUAction, help='name of CSS DLU')


class TestsInitException( Exception ):
    pass

class HealthCallbackHandler( EventCallbackAdapter ):
    def __init__(self, event_name, lk_health, cv_health):
        EventCallbackAdapter.__init__(self)

        self._lk_health = lk_health
        self._cv_health = cv_health
        self._event_name = event_name
        self._health_report = None
        self._health_is_good = True

    def callback(self, mesgName):
        global bad_ill_health
        global health_report

        report = HealthReport(self.getValue())
        health = report.getHealth()
        if self._health_is_good and (
            health == Health.BAD or health == Health.ILL):
            with self._lk_health:
                self._health_is_good = False
                self._health_report = report
                self._cv_health.notify_all()

    def get_health_report(self):
        return self._health_report

    def health_is_good(self):
        return self._health_is_good

class DatStatusCallbackHandler( EventCallbackAdapter ):
    def __init__(self, cmd_line_args, event_name):
        EventCallbackAdapter.__init__(self)
        self._cla = cmd_line_args              # copy command line arguments
        self._event_name = event_name          # copy event name
        self._start_mode = None                # test dependent
        self._received_table = None
        self._dat_acqStatus = None
        self._dat_duration = 0.0
        self._dat_startTime = None             # datetime in isoformat
        self._dat_scheduled_compTime = None    # datetime in isoformat
        self._dat_actual_compTime = None       # datetime in isoformat

        self.WAIT_FOR_WAITING   = 1
        self.WAIT_FOR_ACQUIRING = 2
        self.WAIT_FOR_COMPLETE  = 3
        self.WAIT_FOR_CHECK     = 4
        self._callback_state = self.WAIT_FOR_WAITING
    #
    # End of DatStatusCallbackHandler.__init__()

    def atstdate2datetime( self, timestamp ):
        return datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%S.%fZ') #.isoformat()
    #
    # End of DatStatusCallbackHandler.atstdate2datetime()

    def callback( self, mesgName ):
        self._received_table = self.getValue()
        local_acqStatus = self._received_table.getString( 'dat:acqStatus' )

        if "preview" == local_acqStatus:
            return                              # Ignore preview mode

        if self._callback_state == self.WAIT_FOR_WAITING:
            if local_acqStatus == "waiting":
                self._dat_acqStatus = local_acqStatus
                self._dat_duration = self._received_table.getString('.dat:duration')
                self._dat_startTime = \
                    self.atstdate2datetime(
                        self._received_table.getString('.dat:startTime'))
                self._dat_scheduled_compTime = \
                    self.atstdate2datetime(
                        self._received_table.getString('.dat:scheduledCompTime'))
                self._dat_actual_compTime = None
                self._callback_state = self.WAIT_FOR_COMPLETE
#                self.log_dat_status_info()
        elif self._callback_state == self.WAIT_FOR_COMPLETE:
            if local_acqStatus == "complete":
                self._dat_acqStatus = local_acqStatus
                self._dat_actual_compTime = \
                    self.atstdate2datetime(self._received_table.getString('.dat:actualCompTime'))
                self._callback_state = self.WAIT_FOR_CHECK
#                self.log_dat_status_info()
    #
    # End of DatStatusCallbackHandler.callback()


    def check_dat_status_complete( self, startTime, scheduledCompTime ):
        global SPACER                    # For formatting output
        global SPACER_WIDTH              # For formatting output

        success = True

        # Allowed positive delta between scheduled and actual DAT completion time
        tolerance = timedelta(milliseconds=self._cla.compTolerance)

        # Calculate actual DAT duration from diff between actual completion time
        # and start time from event data
        # Have to use strftime because we always want to display microseconds
        actualDuration = self._dat_actual_compTime - self._dat_startTime
        logging.info(
            '{:>{width}}: Start={}, Completion={}, Duration={}s'.format(
                'Actual',
                self._dat_startTime.strftime("%Y-%m-%dT%H:%M:%S.%f"),
                self._dat_actual_compTime.strftime("%Y-%m-%dT%H:%M:%S.%f"),
                actualDuration,
                width=SPACER_WIDTH))

        try:
            assert 'complete' == self._dat_acqStatus, \
                '{}: ERROR - Expected datStatus.dat:acqStatus={}, actual value={}'.format(
                    SPACER, status, self._dat_acqStatus)

            # NOTE: For startMode="immediate" the start time and completion time
            #       are just approximations because the CSS will automatically
            #       determine the start time. In this case skip the start time
            #       and completion time checks.
            if self._start_mode == StartMode.atStartTime:
                # We have a definitive start time and scheduled completion time
                # so go ahead and check them

                # The start time calculated in this program and sent to the CSS vs
                # the start time being used in the event. They should be the same.
                assert startTime == self._dat_startTime, \
                    '{}: ERROR - Expected datStatus.dat:startTime={}, actual value={}'.format(
                        SPACER, startTime.isoformat(), self._dat_startTime.isoformat())

                # The scheduled completion time calculated in this program vs
                # the scheduled completion time calculated in the CSS and being
                # used in the event. To allow for rounding errors between the
                # two apps allow for a 1 microsecond difference.
                diff_us = (scheduledCompTime - self._dat_scheduled_compTime).microseconds/1000000.0;
                assert math.fabs(diff_us) <= 0.000001, \
                    '{}: ERROR - Expected datStatus.dat:scheduledCompTime={}, actual value={}'.format(
                        SPACER, scheduledCompTime.isoformat(), self._dat_scheduled_compTime.isoformat())

            # Calculate difference between actual and scheduled completion time
            diff = self._dat_actual_compTime - self._dat_scheduled_compTime

            # Determine diff value and sign for screen output
            if diff >= timedelta(microseconds=0):
                diff_for_output = diff
                sign = '+'
            else:
                # invert subtraction so we can output diff as -seconds
                diff_for_output = self._dat_scheduled_compTime - self._dat_actual_compTime
                sign = '-'

            logging.info(
                '{:>{width}}: (Actual Completion-Scheduled Completion)={}{}s, tolerance={}ms'.format(
                    'Difference', sign, diff_for_output,
                    (tolerance.microseconds/1000.0),
                    width=SPACER_WIDTH))

            # Completed late? yes - error
            assert diff <= tolerance, \
                '{:>{width}}: ERROR - DAT did not complete on time!'.format(
                    'Status', width=SPACER_WIDTH)

            # Completed early? yes - error
            assert(diff >= timedelta(milliseconds=0)), \
                '{:>{width}}: ERROR - DAT completed earlier than expected!'.format(
                    'Status', width=SPACER_WIDTH)

            # Just right
            logging.info('{:>{width}}: {}'.format(
                'Status', 'Completed, OK!', width=SPACER_WIDTH))

        except AssertionError as ae:
            logging.critical(ae)
            success = False
        finally:
            self._callback_state = self.WAIT_FOR_WAITING
            return success
    #
    # End of DatStatusCallbackHandler.check_dat_status_complete()

    def log_dat_status_info( self ):
        logging.info(
            '   status={}\n duration={}s\n startTime={}\n schedComp={}\nactualComp={}\n'.format(
            self._dat_acqStatus,
            self._dat_duration,
            self._dat_startTime,
            self._dat_scheduled_compTime,
            self._dat_actual_compTime) )
    #
    # End of DatStatusCallbackHandler.log_dat_status_info()

    def set_start_mode(self, start_mode):
        if not self._start_mode == start_mode:
            self._start_mode = start_mode
            logging.info(
                'DatStatusCallbackHandler - start_mode set to \'{}\''.format(
                    self._start_mode.name()))
    #
    # End of DatStatusCallbackHandler.set_start_mode()

class Tests:
    error = False

    # Class variables
    # Default constructor
    # cmd_line_args:
    # The command line arguments either supplied directly on the command line or
    # read from a JSON Test definition file.
    # startup_state_applied: Flag used to control whehter or not the CSS's
    #                        initial startup configuration will be applied during
    #                        instantiation of this Tests class.
    #                        False - Startup config will be applied
    #                        True - Startup config will not be applied
    def __init__(self,cmd_line_args, startup_state_applied=False):
        global MIN_ABORTCANCEL_WAIT_TIME

        self._cla = cmd_line_args     # copy command line arguments
        self._startup_state_applied = startup_state_applied

        # Using the DLU for this CSS instance get the mgmt controllers name and
        # save and create the accompanying camera controllers name.
        self._mgmt_ctrl_name = AppDB.getDLU(
            self._cla.dluName).getComponents()[0]
        self._cam_ctrl_name = self._mgmt_ctrl_name + '.cam'

        self._dat_id0 = None
        self._num_warnings = 0
        self._num_errors = 0

        # Create a daily referenceT0 as a string in AtstDate form and as
        # fractional seconds since epoch
        self._referenceT0_str = time.strftime("%Y-%m-%dT00:00:00Z",time.gmtime())
        self._referenceT0_sec = AtstDate.parse(
            self._referenceT0_str).getNanoTime() / 1000000000.0

        # Locks and condition vars
        self._lk_health = threading.Lock()  # For use with the health callback
        self._cv_health = threading.Condition(self._lk_health)

        # Event names and callback setup
        self._dat_status_event_name = (
            self._cam_ctrl_name + VCAttributeSets.EVENT_DAT_STATUS)
        self._health_event_name = ReportingHealthServiceTool.HEALTH_EVENT_NAME

        self.vcc = App.connect(self._mgmt_ctrl_name)
        self.vccCam = App.connect(self._cam_ctrl_name)
        self._dat_status_callback = DatStatusCallbackHandler(
            cmd_line_args, self._dat_status_event_name)
        self._health_callback = HealthCallbackHandler(
            self._health_event_name, self._lk_health, self._cv_health)

        # Instantiate test configurations
        self.cfg_previewCcDefault = TestConfiguration()
        self.cfg_previewCcDefault.setFQPrefix(self._mgmt_ctrl_name)

        self.cfg_previewId0 = TestConfiguration()
        self.cfg_previewId0.setFQPrefix(self._mgmt_ctrl_name)

        self.cfg_runId0 = TestConfiguration()
        self.cfg_runId0.setFQPrefix(self._mgmt_ctrl_name)

        # CamConfig and ExecBlock objects
        self.loadedCCs = {}
        self.loadedEBs = {}
        self._css_root_node = None # The CSS's root node of a DAT, points to DAT root node
        self._calc_dat_duration = None  # Value as obtained from assembled DAT
        self._first_cc_in_dat = None;

        # The value for _max_abortcancel_wait will be establishe in assemble_dat()
        # WARNING: If the DAT duration is not specifed with something
        # "reasonable" then a long running DAT can result in an exorbitantly
        # long testing as the random wait time could be upto _calc_dat_duration.
        self._min_abortcancel_wait = MIN_ABORTCANCEL_WAIT_TIME
        self._max_abortcancel_wait = None

        # Generate empty camconfig and execblock attribute tables
        # Add all attribute names for a camConfig, remove the Calculated namespace
        self.atEmptyCC = AttributeTable()
        for name in CamConfig.getRequiredAttributeNames():
            self.atEmptyCC.insert(Attribute(name,''))
        for name in CamConfig.getOptionalAttributeNames():
            self.atEmptyCC.insert(Attribute(name,''))
        self.atEmptyCC.extractOnPrefix(Calculated.getNamespace())

        # Add all attribute names for an execBlock.
        self.atEmptyEB = AttributeTable()
        for name in ExecBlock.getRequiredAttributeNames():
            self.atEmptyEB.insert(Attribute(name,''))
        for name in ExecBlock.getOptionalAttributeNames():
            self.atEmptyEB.insert(Attribute(name,''))

        #=======================================================================
        #
        # Start initializing things from the running CSS...
        # First, we need to retrieve the startup configuration submitted by the
        # Mgmt controller to the Cam controller during startup. We get at this
        # by retrieving the timestamp that startup completed and then use
        # CatWriter to get the configuation.
        at = AttributeTable()
        at.setFQPrefix(self._mgmt_ctrl_name)
        at = at.requalify(self._mgmt_ctrl_name)

        # Get the camera ID info and the timestamp of when startup completed...
        at.insert(Global.CAMERA_ID)
        at.insert(VCAttributeSets.TESTING_STARTUP_COMPLETED_AT)

        at = self.vcc.get(at)
        at.setFQPrefix(self._mgmt_ctrl_name)

        self._camera_id = CameraId.create(at.getStringArray(Global.CAMERA_ID))
        self._css_startup_completed_at = at.getString(
            VCAttributeSets.TESTING_STARTUP_COMPLETED_AT)

        # If we're running a NIRSP camera then we will need to retrieve sampling
        # attributes that could be submitted as part of a .dat or .sds file. We
        # need this in order to set the sampling in our CamConfig Factory.
        self._at_sampling = None
        self._is_nirsp_camera = False
        if (CameraIdSets.isCryoNirspCameraId(self._camera_id) or
            CameraIdSets.isDlNirspCameraId(self._camera_id)):
            self._is_nirsp_camera = True
            self._at_sampling = AttributeTable()
            self._at_sampling.setFQPrefix(self._mgmt_ctrl_name)
            self._at_sampling = self._at_sampling.requalify(self._mgmt_ctrl_name)
            self._at_sampling.insert(Sampling.MODE)
            self._at_sampling.insert(Sampling.NUM_NDRS)
            self._at_sampling.insert(Sampling.ACCUM_SYNC)
            if CameraIdSets.isDlNirspCameraId(self._camera_id):
                self._at_sampling.insert(Sampling.SEQUENCE)
            elif not CameraIdSets.isCryoNirspCameraId(self._camera_id):
                # For some future NIRSP
                raise TestsInitException('UNKNOWN CAMERA {}'.format(
                    self._camera_id.cameraName()))

        # Using CatWriter get the most current configuration preceeding the time
        # that the CSS completed startup and convert it to an attribute table.
        # This is the initial config submitted by the Mgmt controller to the Cam
        # controller to get things rolling at startup.
        try:
            configAttributes = subprocess.check_output(
                ["CatWriter --endDate={} --n=1 --attributesOnly --pretty=false".format(
                    self._css_startup_completed_at)],
                shell=True)

            newAt = JsonUtil.json2AttributeTable(configAttributes)

        except subprocess.CalledProcessError as cpe:
            err = 'subprocess.check_output error: error code={}, msg={}'.format(
                cpe.returncode, cpe.output)
            raise TestsInitException(err)
        except(Exception, JavaException) as ex:
            raise TestsInitException("{}\n{}".format(
                traceback.format_tb(sys.exc_info()[2], limit=10),
                ex.getCause()))

        # Requalify to the cam controller namespace as this is the namespace
        # that the startup config was submitted to by the Mgmt controller.
        # Then, remove attributes that either can't be submitted from the
        # outside world or that we don't want to submit as part of a reset...
        newAt.setFQPrefix(self._cam_ctrl_name)
        newAt.extractOnPrefix(Calculated.getNamespace())
        newAt.extractOnPrefix(VCAttributeSets.INTERNAL_RUN_CAMERA)
        newAt.extractOnPrefix(Sampling.SEQUENCE_DETAILS)
        newAt.extractOnPrefix(Global.ID0_LIST)
        newAt.extractOnPrefix(Global.ID0_DURATION)
        newAt.extractOnPrefix(Global.LINCORR_TABLE_NAME)
        if len(newAt.getString(Global.PASS_THROUGH_METADATA)) == 0:
            newAt.extractOnPrefix(Global.PASS_THROUGH_METADATA)

        # Requalify all attributes back to the Mgmt controller namespace so that
        # later on, when a "reset" is needed, we can use this attribute table
        # to create a config to be submitted to the Mgmt controller
        newAt.setFQPrefix(self._mgmt_ctrl_name)
        self._at_css_startup = newAt.requalify(
            self._cam_ctrl_name, self._mgmt_ctrl_name)

        if self._cla.verbose:
            logging.info('CSS Startup Config(pruned):')
            self._at_css_startup.show("\t")

        # Based on the initial configuration, get and save some attributes
        self._default_cc_id = self._at_css_startup.getString(Global.ID0)

        # If the test object was constructued with startup_state_applied == False
        # then return to the CSS startup state
        if (not self.get_startup_state_applied() and
            not self._return_to_startup_state()):
            raise TestsInitException('Failure applying startup config!')

        logging.info("Proceeding with tests...")

        self.ccFactory = CamConfigFactory(self._camera_id)

        # If we're running with the Cryo-NIRSP or DL-NIRSP camera then we need
        # to set the current sampling info, as configured on the camera, in the
        # CamConfigFactory...
        if self._is_nirsp_camera:
            self.ccFactory.setSampling(Sampling.create(self._at_css_startup))

        # When startMode="atStartTime", this is the offset from "now" to when
        # the DAT should start.
        if self._cla.startDelay is None:
            self._start_delay = 4.0
            if CameraIdSets.isCryoNirspCameraId(self._camera_id):
                # For CryoNIRSP we need to base the start delay on the defined
                # 'stop' and 'set' timing values from the camera properties plus
                # some time code processing(csf, 'info', etc.')
                at.clear()
                at.setFQPrefix(self._mgmt_ctrl_name)
                at = at.requalify(self._mgmt_ctrl_name)

                # Get the 'set' and 'stop' command timing values...
                at.insert(VCAttributeSets.CSH_TIMING_SET)
                at.insert(VCAttributeSets.CSH_TIMING_STOP)

                at = self.vcc.get(at)
                at.setFQPrefix(self._mgmt_ctrl_name)

                # Set the delay as the ceil to give a bit more time....
                self._start_delay = math.ceil(
                    at.getDouble(VCAttributeSets.CSH_TIMING_SET) +
                    at.getDouble(VCAttributeSets.CSH_TIMING_STOP) + 1.0)

        else:
            self._start_delay = self._cla.startDelay

    #
    # End of Tests.__init__()

    def delete_all_ids(self):
        success = True
        txt = 'Deleting all loaded Execution Block IDs...'
        logging.info(txt)

        cfg = TestConfiguration()
        cfg.setFQPrefix(self._mgmt_ctrl_name)
        cfg.insert(Global.DELETE_ID, 'allExecBlocks')
        ar = self._submit_config(cfg)
        if ar == ActionResponse.OK:
            logging.info('{}OK!'.format(txt))
        else:
            logging.error(
                '{}Failure deleting all Execution Blocks - {}: {}'.format(
                    txt, ar.errString(), ar.getReason()))
            self._num_errors += 1
            success = False

        txt = 'Deleting all loaded Camera Configuration IDs...'
        logging.info(txt)

        cfg2 = TestConfiguration()
        cfg2.setFQPrefix(self._mgmt_ctrl_name)
        cfg2.insert(Global.DELETE_ID, 'allCamConfigs')
        ar = self._submit_config(cfg2)
        if ar == ActionResponse.OK:
            logging.info('{}OK!\n'.format(txt))
        else:
            logging.error(
                '{}Failure deleting all Camera Configurations - {}: {}\n'.format(
                    txt, ar.errString(), ar.getReason()))
            self._num_errors += 1
            success = False

        return success
    #
    # End of Tests.delete_all_ids()

    def _get_ccs_and_ebs(self, listOfIds):
        logging.info('LIST OF IDS TO GET: [{}]'.format(
            ', '.join(str(id) for id in listOfIds)))
        for id in listOfIds:
            if id == self._default_cc_id: continue

            if CamConfigId.isCandidateId(id):
                # Create and merge a camConfig ID
                at = copy.copy(self.atEmptyCC)
                at.merge(CamConfigId.create(id).toAttributeTable())

            else:
                # Create and merge an execution block ID
                at = copy.copy(self.atEmptyEB)
                at.merge(ExecBlockId.create(id).toAttributeTable())

            # Get the attributes for the CamConfig or ExecBlock
            at = self.vcc.get(at.requalify(self._mgmt_ctrl_name))
            at.setFQPrefix(self._mgmt_ctrl_name)

            # Create the object and add it to the dictionary of CCs or EBs
            if CamConfigId.isCandidateId(id):
                cc = self.ccFactory.create(at)
                self.loadedCCs[cc.ID()] = cc
            else:
                eb = ExecBlock(at,None)
                self.loadedEBs[eb.ID()] = eb
    #
    # End of Tests._get_ccs_and_ebs()

    def get_num_warnings(self):
        return self._num_warnings
    #
    # End of Tests.get_num_warnings()

    def get_num_errors(self):
        return self._num_errors
    #
    # End of Tests.get_num_errors()

    def get_startup_state_applied(self):
        return self._startup_state_applied
    #
    # End of Tests.get_startup_state_applied

    def _get_wait_time(self, wait_type):
        global MIN_GENERIC_WAIT_TIME
        global MAX_GENERIC_WAIT_TIME
        global WAIT_ABORTCANCEL       # Wait for random time based on DAT duration
        global WAIT_GENERIC           # Wait for random time based on fixed range

        if wait_type == WAIT_GENERIC:
            waitTime = random.uniform( MIN_GENERIC_WAIT_TIME,
                                       MAX_GENERIC_WAIT_TIME )

        elif wait_type == WAIT_ABORTCANCEL:
            waitTime = random.uniform( self._min_abortcancel_wait,
                                       self._max_abortcancel_wait )
        else:
            logging.error('Unknown \'type\'=\'{}\' in _get_wait_time()'.format(
                wait_type))
            exit

        return waitTime
    #
    # End of Tests._get_wait_time()

    def assemble_dat(self):
        success = True
        self._first_cc_in_dat = None;
        self._css_root_node = RootNode( self._dat_id0, 1 )
        self._do_assemble( 0, self._css_root_node)
        self._calc_dat_duration = self._css_root_node.getDuration()

        logging.info(
            'Tree view of DAT, Calculated DAT duration: {:.06f} sec == {}\n{}\n'.format(
            self._calc_dat_duration,
            timedelta(seconds=self._calc_dat_duration),   # sec and HH:MM:SS.sss
            self._css_root_node.toTreeDiagramString().encode('utf8')))


        # If a DAT duration wasn't specified on the comand line, use the 
        # actual calculated DAT duration. Otherwise, the max wait time
        # was establish using the specified command line value.
        # See WARNING in  __init__ regarding DAT duration and wait times.
        if self._cla.datDuration is None:
            # Duration was not defined on the command line. Use actual duration
            self._max_abortcancel_wait = self._calc_dat_duration
        elif self._cla.datDuration <= self._min_abortcancel_wait:
            logging.error(
                '--datDuration {}, value must be > {}'.format(
                    self._cla.datDuration, self._min_abortcancel_wait))
            self._num_errors += 1
            success = False
        elif self._cla.datDuration > self._calc_dat_duration:
            logging.error(
                '--datDuration {} exceeds actual DAT duration of {}'.format(
                    self._cla.datDuration, self._calc_dat_duration))
            self._num_errors += 1
            success = False
        else:
            # command line value is in range 
            self._max_abortcancel_wait = self._cla.datDuration

        return success
    #
    # End of Tests.assemble_dat()

    # Recursively construct the DAT by adding DATComponents to
    # the execution blocks. Once complete, the DATComponents are
    # used to recursively calculate the DAT duration and to print
    # a tree construct of the DAT.
    def _do_assemble(self, dataTier, execBlock):
        execBlock.clearDATComponents()

        dataTier += 1
        # For all nodes in this exeution block, add an appropriate
        # DATComponent for the exeuction blocks, CC's, and or CcNoOp
        # that this execution block names.
        for node in execBlock.exec().nodes():
            idToLoad = node.nextID()
            if ExecBlockId.isCandidateId( idToLoad ):
                eb = self.loadedEBs[idToLoad]
                execBlock.addDATComponent( eb )
                self._do_assemble( dataTier, eb )
            elif CamConfigId.create(idToLoad).isReservedId():
                execBlock.addDATComponent( CcNoOp.create() )
            else:
                cc = self.loadedCCs[idToLoad]
                execBlock.addDATComponent( cc )
                if self._first_cc_in_dat is None:
                    self._first_cc_in_dat = cc
    #
    # End of Tests._do_assemble()

    def load_dat(self):
        success = True

        cmd = ('ajava atst.css.tools.CssCcEbTools\\$SubmitDirect' +
              ' --vcName=' + self._mgmt_ctrl_name +
              ' --fileName=' + self._cla.datFile +
              ' --fqPrefix=' + self._cla.source)
        logging.info(cmd)
        shellCmd = ShellCommand(cmd)
        if not shellCmd.execute() == 0: 
            # The shell command failed most likely due to some error during 
            # submit of the CC's and EB's
            logging.critical(shellCmd.getStderr())
            self._num_errors += 1
            success = False
            return success

        # Get the list of IDs now loaded
        at = AttributeTable()
        at.insert(Global.ID_LIST)
        at = at.requalify(self._mgmt_ctrl_name)

        # If this is a NIRSP type camera then also get the current sampling
        # attributes as they may have be modified during the submit above.
        if self._is_nirsp_camera:
            at.merge(self._at_sampling)

        # Do the actual get and then reset the FQPrefix on the returned table.
        at = self.vcc.get(at)
        at.setFQPrefix(self._mgmt_ctrl_name)

        # If this is a NIRSP camera then apply the sampling attributes to our
        # instance of CamConfigFactory.
        if self._is_nirsp_camera:
            self.ccFactory.setSampling(Sampling.create(at))

        listOfIds = at.getStringArray(self._mgmt_ctrl_name + Global.ID_LIST)
        if self._cla.datId0 is None:
            # Find id containing 'root' in its name. This will be ID0.
            for id in listOfIds:
                logging.info(id)
                if id.find('root') >= 0:
                    self._dat_id0 = id
        else:
            # Verify named datId0 is actually in the list of loaded IDs.
            for id in listOfIds:
                logging.info(id)
                if id.find(self._cla.datId0) >= 0:
                    self._dat_id0 = id

        if self._dat_id0 is None:
            if self._cla.datId0 is None:
                logging.error(
                    'Cannot find an ExecBlock ID containing \"root\" in its name.')
            else:
                logging.errlr(
                    'Cannot find datId0=\"{}\" in list of loaded IDs.'.format(
                        self._cla.datId0))
            self._num_errors += 1
            success = False

        else:      
            logging.info('ROOT ID: {}'.format(self._dat_id0))
            logging.info('DEFAULT: {}'.format(self._default_cc_id))
            self._get_ccs_and_ebs(listOfIds)

        return success
    #
    # End of Tests.load_dat()

    def _return_to_startup_state(self):
        success = True
        logging.info("Returning CSS to startup state...")

        id0 = self._at_css_startup.getString(Global.ID0)
        cfg = TestConfiguration()
        cfg.setFQPrefix(self._mgmt_ctrl_name)
        cfg.merge(self._at_css_startup)

        txt = '    Restoring initial configuration...'.format(id0)
        logging.info(txt)

        ar = self._submit_config(cfg)
        if ar == ActionResponse.OK:
            logging.info(
                '{}OK - \"{}\" restarted successfully!'.format(txt,id0))
            logging.info('CSS startup state restored!\n')

            # We track whether the startup state has been applied so that if
            # we're looping through test scenarios we don't waste time
            # needlessly re-applying the startup state.
            self._startup_state_applied = True

        else:
            success = False
            logging.error(
                '{}Failure restoring initial configuration - {}: {}\n'.format(
                    txt, ar.errString(), ar.getReason()))

        return success
    #
    # End of Tests._return_to_startup_state()

    def _submit_config(self, config):
        ra = BaseMisc.submitAndWait(NullAction(), self.vcc, config)
        ar = ra.getActionResponse()

        # Anytime a config is sumbitted the CSS has potentially been altered
        # and, as far as this method is concerned, the startup state is no
        # longer in effect.
        self._startup_state_applied = False

        return ar
    #
    # End of Tests._submit_config()

    def _do_pre_run_sequence(self,testNum):
        global PRE_PREVIEW_CCDEFAULT     # Execute default CC in preview
        global PRE_RESUBMIT_ID0          # Resubmit all CC's/EB's of DAT
        global PRE_PREVIEW_ID0           # Execute DAT in preview
        global PRE_DELAY                 # Add random delay based on fixed range
        global SPACER                    # For formatting output
        global WAIT_GENERIC              # Wait for random time based on fixed range

        # Create some ID strings for output...
        id0String = "DAT with ID0={}".format(\
            self.cfg_previewId0.getString(self._mgmt_ctrl_name + Global.ID0))
        ccDefaultString = "DAT with ID0={}".format(\
            self.cfg_previewCcDefault.getString(self._mgmt_ctrl_name + Global.ID0))

        success = True
        for action in self._cla.preRunSequence:
            if action == PRE_PREVIEW_CCDEFAULT:
                # Preview the default CC. This may be the a cc_default deifned in the 
                # loaded DAT file or it could be the one loaded by default by the CSS.
                logging.info('{}: Previewing {}'.format(
                    SPACER, ccDefaultString))
                ar = self._submit_config(self.cfg_previewCcDefault)
                if not ar == ActionResponse.OK:
                    logging.error('{}: {}'.format(
                        ar.errString(), ar.getReason()))
                    self._num_errors += 1
                    success = False

            elif action == PRE_RESUBMIT_ID0:
                logging.info(
                    '{}: Modifying and resubmitting all CCs and EB for {}'.format(
                        SPACER, id0String))
                success = self._resubmit_id0(testNum)

            elif action == PRE_PREVIEW_ID0:
                # Preview the DAT...
                logging.info('{}: Previewing {}'.format(
                    SPACER, id0String))
                ar = self._submit_config(self.cfg_previewId0)
                if not ar == ActionResponse.OK:
                    self._num_errors += 1
                    success = False
                    logging.error('{}: {}'.format(
                        ar.errString(), ar.getReason()))

            elif action == PRE_DELAY:
                timeToWait =  self._get_wait_time(WAIT_GENERIC)
                logging.info(
                    '{}: Delaying {:.3f}sec before continuing...'.format(
                        SPACER,timeToWait))
                success = self.do_wait(timeToWait)

            if not success: break

        return success
    #
    # End of Tests._do_pre_run_sequence()

    def _resubmit_id0(self,testNum):
        # Resubmit all CC's and EB's of the DAT.
        # Modify the "descripition" by appending ' - {num}' to the original value
        # so that the CC or EB will have new content.
        success = True
        for ccId in self.loadedCCs:
            cc = self.loadedCCs[ccId]
            newDescription = cc.description() + " - {}".format(testNum)
            newCC = cc.withDescription(newDescription)
            cfg = TestConfiguration()
            cfg.setFQPrefix(self._mgmt_ctrl_name)
            cfg.merge(newCC.toAttributeTable().requalify(self._mgmt_ctrl_name))
            ar = self._submit_config(cfg)
            if not ar == ActionResponse.OK:
                self._num_errors += 1
                success = False
                loging.error(
                    'Failure submitting configuration for {} - {}: {}'.format(
                        newCC.ID(), ar.errString(), ar.getReason()))

        for ebId in self.loadedEBs:
            eb = self.loadedEBs[ebId]
            newDescription = eb.description() + " - {}".format(testNum)
            cfg = TestConfiguration()
            cfg.setFQPrefix(self._mgmt_ctrl_name)
            cfg.merge( eb.toAttributeTable().requalify(self._mgmt_ctrl_name) )
            cfg.insert( Attribute(ExecBlock.DESCRIPTION, newDescription))
            cfg.requalify(self._mgmt_ctrl_name)
            ar = self._submit_config(cfg)
            if not ar == ActionResponse.OK:
                self._num_errors += 1
                success = False
                logging.error(
                    'Failure submitting configuration for {} - {}: {}'.format(
                        eb.ID(), ar.errString(), ar.getReason()))
        return success
    #
    # End of Tests._resubmit_id0()

    def initialize_test_configs(self, start_mode, trigger_reference):
        # Configurations for setting global attributes
        self.cfg_previewCcDefault.insert(Global.CAM_MODE, CamMode.preview.name())
        self.cfg_previewCcDefault.insert(Global.ID0, self._default_cc_id)
        self.cfg_previewCcDefault.insert(Global.EXEC_COUNT0, 0)
        self.cfg_previewCcDefault.insert(Global.START_MODE, StartMode.immediate.name())
        self.cfg_previewCcDefault.insert(Global.TRIGGER_REFERENCE, trigger_reference.name())
        self.cfg_previewCcDefault.insert(Global.REFERENCE_T0, self._referenceT0_str)

        self.cfg_previewId0.setFQPrefix(self._mgmt_ctrl_name)
        self.cfg_previewId0.insert(Global.CAM_MODE, CamMode.preview.name())
        self.cfg_previewId0.insert(Global.ID0, self._dat_id0)
        self.cfg_previewId0.insert(Global.EXEC_COUNT0, 0)
        self.cfg_previewId0.insert(Global.START_MODE, StartMode.immediate.name())
        self.cfg_previewId0.insert(Global.TRIGGER_REFERENCE, trigger_reference.name())
        self.cfg_previewId0.insert(Global.REFERENCE_T0, self._referenceT0_str)

        self.cfg_runId0.setFQPrefix(self._mgmt_ctrl_name)
        self.cfg_runId0.insert(Global.CAM_MODE, self._cla.camMode)
        self.cfg_runId0.insert(Global.ID0, self._dat_id0)
        self.cfg_runId0.insert(Global.EXEC_COUNT0, self._cla.execCount)
        self.cfg_runId0.insert(Global.START_MODE, start_mode.name())
        self.cfg_runId0.insert(Global.TRIGGER_REFERENCE, trigger_reference.name())
        self.cfg_runId0.insert(Global.REFERENCE_T0, self._referenceT0_str)
    #
    # End of Tests.initialize_test_configs()

    def show_setup(self):
        totalRunTime = self._calc_dat_duration * self._cla.execCount

        logging.info(
            '{}\n'
            '{: >58}: {}\n'\
            '{: >58}: {}\n'\
            '{: >58}: {}\n'\
            '{: >58}: {}\n'\
            '{: >58}: {}\n'\
            '{: >58}: {}\n'\
            '{: >58}: {}\n'\
            '{: >58}: {}\n'\
            '{: >58}: {:.3f} sec\n'\
            '{: >58}: {}\n'\
            '{: >58}: {}\n'\
            '{: >58}: \'{}\'\n'\
            '{: >58}: {}\n'\
            '{: >58}: {:.6f} sec == {} (single pass)\n'\
            '{: >58}: {:.6f} sec == {} (Calc. DAT Duration * execCount)\n'\
            '{: >58}: {:.3f} ms\n'\
            '{: >58}: {:.3f}/{:.3f} sec (for abort/cancel tests)\n'.format(
                ' All Test Parameters '.center(80,'*'),
                'Input DAT File', self._cla.datFile,
                'CSS Mgmt Ctrl', self._mgmt_ctrl_name,
                'CSS Cam  Ctrl', self._cam_ctrl_name,
                'Modes to Test', self._cla.modesToTest,
                'numPasses', self._cla.numPasses,
                'fastAsPossible', str(self._cla.fastAsPossible),
                'preRunSequence', self._cla.preRunSequence,
                'previewCcDefault', str(self._cla.previewCcDefault),
                'datDuration', self._cla.datDuration,
                'triggerReference', self._cla.triggerReference,
                'startMode', self._cla.startMode,
                'camMode', self._cla.camMode,
                'execCount', self._cla.execCount,
                # Both Calc DAT Duration and Total runtime will displaay
                # time in seconds and in HH:MM::SS.sss
                'Calc. DAT Duration', self._calc_dat_duration,
                                      timedelta(seconds=self._calc_dat_duration),
                'Total Run Time', totalRunTime,
                                  timedelta(seconds=totalRunTime),
                'Completion Tolerance', self._cla.compTolerance,
                'Min/Max wait time', self._min_abortcancel_wait,
                                     self._max_abortcancel_wait))
    #
    # End of Tests.show_setup()

    def execute_preview_test(self, trigger_reference):
        global SPACER             # For formatting output
        global WAIT_GENERIC       # Wait for random time based on fixed range

        testName = 'previewTest'

        if self._cla.numPasses == 1:
          logging.info('Executing {} {} time...'.format(
            testName, self._cla.numPasses))
        else:
          logging.info('Executing {} {} times...'.format(
            testName, self._cla.numPasses))

        cfg = TestConfiguration()
        cfg.setFQPrefix(self._mgmt_ctrl_name)
        cfg.insert(Global.CAM_MODE, CamMode.preview.name())
        cfg.insert(Global.EXEC_COUNT0, 0)
        cfg.insert(Global.START_MODE, StartMode.immediate.name())
        cfg.insert(Global.TRIGGER_REFERENCE, trigger_reference.name())
        cfg.insert(Global.REFERENCE_T0, self._referenceT0_str)

        id0List = [self._default_cc_id, self._dat_id0]

        # Set the DatStatusCallbackHandler with the current start mode even
        # though we're only in preview mode.
        self._dat_status_callback.set_start_mode(StartMode.immediate)

        #
        # 'preview test'
        #


        try:
            # Always subscribe to the health event...
            Event.subscribe(self._health_event_name, self._health_callback )

            success = True
            for i in range(self._cla.numPasses):
                pass_info_txt = 'Pass {:03d} of {:03d}'.format(
                    (i+1), self._cla.numPasses)
                for idx in range(2):
                    if idx == 1:
                        pass_info_txt = SPACER

                    logging.info('{}: Starting DAT with ID0={}'.format(
                        pass_info_txt, id0List[idx]))

                    cfg.insert(Global.ID0, id0List[idx])
                    ar = self._submit_config(cfg)
                    if not ar == ActionResponse.OK:
                        success = False
                        logging.error('{}: {}'.format(
                            ar.errString(), ar.getReason()))
                        break

                    # Let DAT run a "random" amount of time before proceeding...
                    success = self.do_wait(self._get_wait_time(WAIT_GENERIC))

                    if not success: break;

                if not success: break

        finally:
            # All done with the health event...
            Event.unsubscribe(self._health_event_name, self._health_callback )

        if success:
            logging.info('{}: Success!'.format(testName))
        else:
            self._num_errors += 1
            logging.error('{}: Failure!'.format(testName))
        logging.info('')

        return success
    #
    # End of Tests.execute_preview_test()

    def execute_dat_test(self, mode, start_mode, trigger_reference):
        global ACTION_ABORT       # Abort DAT after some period of time
        global ACTION_CANCEL      # Cancel DAT after some period of time
        global ACTION_NONE        # Take no action
        global MODE_ABORT         # From --mode: Start DAT and issue abort before DAT completes
        global MODE_CANCEL        # From --mode: Start DAT and issue cancel before DAT completes
        global MODE_NO_ACTION     # From --mode: Start DAT and let it run to completion
        global SPACER             # For formatting output

        success = True
        testName = 'datTest'
        if self._cla.numPasses == 1:
            logging.info('Executing {} {} time with mode={}...'.format(
                testName, self._cla.numPasses, mode))
        else:
            logging.info('Executing {} {} times with mode={}...'.format(
                testName, self._cla.numPasses, mode))

        # Set the DatStatusCallbackHandler with the current start mode which it
        # will use when determining DAT completion time status.
        self._dat_status_callback.set_start_mode(start_mode)

        for i in range(self._cla.numPasses):

            if mode == MODE_ABORT:
                action = ACTION_ABORT
            elif mode == MODE_CANCEL:
                action = ACTION_CANCEL
            elif mode == MODE_NO_ACTION:
                action = ACTION_NONE
            else:
                logging.error('Unknown mode {} in execute_dat_test()'.format(
                    mode))
                exit

            logging.info('Pass {:03d} of {:03d}: {}'.format(
                (i+1), self._cla.numPasses, action))

            # Execute the pre-run sequence (if defined)
            if not self._cla.preRunSequence == None:
                success = self._do_pre_run_sequence(i)
                if not success: break

            success = self.do_run_dat(action,i, start_mode, trigger_reference)
            if not success: break

            success = self.do_post_dat()
            if not success: break
        
            logging.info('{}: Test Complete'.format(SPACER))

        if success:
            logging.info('{}, mode={}: Success!\n'.format(testName, mode))
        else:
            logging.error('{}, mode={}: Failure!\n'.format(testName, mode))

        return success
    #
    # End of Tests.execute_dat_test()

    def do_post_dat(self):
        global SPACER             # For formatting output
        success = True

        # The DAT test should have just completed and returned to preview mode
        # If previewCcDefault is true switch the DAT back to cc_default.
        # Otherwise, leave the DAT alone and let it idle in preview mode.
        actionStatus = '{}: Done'.format(SPACER)
        if self._cla.previewCcDefault:
            datId0String = 'DAT with ID0={}'.format(self._default_cc_id)

            logging.info('{}: Switching to {}'.format(SPACER, datId0String))


            ar = self._submit_config(self.cfg_previewCcDefault)
            if ar == ActionResponse.OK:
                logging.info('{}, {} is idling in \'preview\' mode'.format(
                    actionStatus, datId0String))
            else:
                self._num_errors += 1
                success = False
                logging.info('{}: {}'.format(ar.errString(), ar.getReason()))
        else:
            datId0String = 'DAT with ID0={}'.format(self._dat_id0)
            logging.info('{}, {} is idling in \'preview\' mode'.format(
                actionStatus, datId0String))


        return success
    #
    # End of Tests.do_post_dat()

    def do_run_dat(self, action, testNum, start_mode, trigger_reference):
        global ACTION_ABORT       # Abort DAT after some period of time
        global ACTION_CANCEL      # Cancel DAT after some period of time
        global ACTION_NONE        # Take no action
        global SPACER             # For formatting output
        global WAIT_ABORTCANCEL   # Wait for random time based on DAT duration
        global WAIT_GENERIC       # Wait for random time based on fixed range
        success = True
        datId0String = 'DAT with ID0={}'.format(self._dat_id0)

        # Get a random amount of time to wait for an abort/cancel test
        timeToWait = self._get_wait_time(WAIT_ABORTCANCEL)

        # Check if startMode is 'immediate' or 'atStartTime' and setup
        # start time and expected completion time.
        # NOTE: .global:startMode was configured in __init__ and remains
        #       a constant for the duration of execution of this program

        try:
            # Always subscribe to the health event...
            Event.subscribe(self._health_event_name, self._health_callback )

            if action == ACTION_NONE:
                logging.info('{}: Starting {}, action to take: {}'.format(
                    SPACER, datId0String, action))

            elif action == ACTION_CANCEL or action == ACTION_ABORT:
                logging.info('{}: Starting {}, action to take: {} after {:06f}sec'.format(
                    SPACER, datId0String, action, timeToWait))

            timeNow = datetime.utcnow()
            if StartMode.immediate == start_mode:
                startTime = timeNow
                extraText = '(Times are approximate due to startMode=\'immediate\')'

            elif StartMode.atStartTime == start_mode:
                # Pick start time '_start_delay' seconds in the future on
                # an 1 second boundary.
                startTime = timeNow.replace(microsecond=0) + \
                            timedelta(seconds=self._start_delay)
                extraText = ''

                if TriggerReference.absolute == trigger_reference:
                    # We need a timedelta object to get at the seconds. Taking the diff
                    # between epoch and the start time will give us that
                    epoch = datetime.utcfromtimestamp(0)
                    startTime_sec = (startTime - epoch).total_seconds()

                    # Calculate the next t0 referenced trigger following the selected
                    # start time. Return is in nanoseconds so convert to seconds
                    nextTriggerTime_ns = TriggerCalculator.calculateNextTriggerTime(
                        self._referenceT0_sec, startTime_sec, self._first_cc_in_dat )
                    nextTriggerTime_sec = nextTriggerTime_ns / 1000000000.0

                    # Take nextTriggerTime (in seconds) and covert to a UTC timestamp
                    # and update the start time to use this value. This assures it is
                    # on a t0 referenced boundary.
                    startTime = datetime.utcfromtimestamp(nextTriggerTime_sec)

                # If this is an abort/cancel test then we want the abort/cancel
                # to occur somtime after the startTime. Add the _start_delay
                # to the current timeToWait value obtained on entry to this method.
                timeToWait += self._start_delay

                # Set the start time in the config being sent to the CSS
                self.cfg_runId0.insert(
                    Global.INITIAL_START_TIME, '{}'.format(startTime.isoformat()) )

            else:
                # This is a 'just in case' a new start mode is ever added to the CSS
                raise ValueError(
                    'No support for startMode={}'.format(start_mode.name()))

            # Calculate expected end time based on start + dat duration
            totalDuration = timedelta(seconds=self._calc_dat_duration * self._cla.execCount)
            expectedEndTime = startTime + totalDuration

            # Have to use strftime because we always want to display microseconds
            logging.info(
                '{:>{width}}: Start={}, Completion={}, Duration={}s {}'.format(
                    'Scheduled',
                    startTime.strftime("%Y-%m-%dT%H:%M:%S.%f"),
                    expectedEndTime.strftime("%Y-%m-%dT%H:%M:%S.%f"),
                    totalDuration, extraText, width=SPACER_WIDTH))

            if action == ACTION_NONE:
                # Subscribe to the datStatus event...
                Event.subscribe(
                    self._dat_status_event_name, self._dat_status_callback )

                # Let DAT run to completion. Submit and wait for completion...
                ar = self._submit_config(self.cfg_runId0)
                actualEndTime = datetime.utcnow()      # Based on receipt of ActionResponse

                # As soon as we get the ActionResponse unsubscribe from the
                # datStatus event.
                Event.unsubscribe(
                    self._dat_status_event_name, self._dat_status_callback )

                # Check the ActionResponse...
                assert(ar == ActionResponse.OK),\
                    '{}: {}'.format(ar.errString(), ar.getReason())

                # Check datStatus event info for valid start time, completion
                # time, and duration...
                assert(self._dat_status_callback.check_dat_status_complete(
                    startTime, expectedEndTime)),\
                    'Error occurred in action \'{}\', dat status check!'.format(
                        action)

            elif action == ACTION_CANCEL or action == ACTION_ABORT:
                # Start the DAT and then wait a random amount of time before
                # issuing the cancel/abort
                ra = BaseMisc.submit(NullAction(), self.vcc, self.cfg_runId0)

                # Wait a short, random amount of time before going on
                # Use of do_wait() will monitor health while we wait...
                assert(self.do_wait(timeToWait)),\
                    'Error occurred in action \'{}\', do_wait() #1'.format(
                        action)

                timeNow = datetime.utcnow()   # grab time when abort/canel is issued
                actionState = ''
                if action == ACTION_CANCEL:
                    # Issue cancel and wait for completion.
                    # Use of do_wait() will monitor health while we wait...
                    logging.info('{:>{width}}: Issuing \'cancel\'...'.format(
                        'Action', width=SPACER_WIDTH))

                    ra.cancel()
                    while ra.getState() is not RemoteActionState.DONE:
                        assert(self.do_wait(0.01)),\
                        'Error occurred while waiting for \'cancel\' to complete!'
                    actionState = 'Canceled'

                else:
                    # Issue abort and wait for completion.
                    # Use of do_wait() will monitor health while we wait...
                    logging.info('{:>{width}}: Issuing \'abort\'...'.format(
                        'Action', width=SPACER_WIDTH))

                    ra.abort()
                    while ra.getState() is not RemoteActionState.ABORTED:
                        assert(self.do_wait(0.01)),\
                        'Error occurred while waiting for \'abort\' to complete!'
                    actionState = 'Aborted'

                logging.info('{:>{width}}: {} at {}'.format(
                    'Status',
                    actionState,
                    timeNow.isoformat(), width=SPACER_WIDTH))
              
            # At this point the DAT either ran to completion or was canceled or
            # aborted.
            if not self._cla.fastAsPossible:
                # Idle the DAT a short, random amount of time before going on
                timeToWait = self._get_wait_time(WAIT_GENERIC)
                logging.info(
                    '{}: Idling {} in \'preview\' mode for {:.03} sec'.format(
                        SPACER, datId0String, timeToWait))

                assert(self.do_wait(timeToWait)),\
                    'Error occurred during do_wait() #2 following \'{}\'!'.format(
                        action)

            else:
                logging.info('{}: {} is idling in \'preview\' mode'.format(
                    SPACER, datId0String))

        except ValueError as ve:
            self._num_errors += 1
            success = False
            logging.error(ve)

        except Exception as ae:   # Generally being used by assert() checks...
            self._num_errors += 1
            success = False
            logging.critical(ae)

        finally:
            # All done with the health event...
            Event.unsubscribe(self._health_event_name, self._health_callback )

        return success
    #
    # End of Tests.do_run_dat()

    def do_wait(self, timeToWait):
        success = True

        # Wait the specified amount of time or until notified of a health change
        with self._cv_health:
            self._cv_health.wait(timeToWait)

        if not self._health_callback.health_is_good():
            report = self._health_callback.get_health_report()
            success = False
            logging.critical(
                "HEALTH went {} during do_wait({:0.3f}s), aborting test!\n"
                "  Reason: {}".format(
                    report.getHealth(),timeToWait,report.getReason()))

        return success
    #
    # End of Tests.do_wait()

    # Wrap up the tests by logging counts/completion status and return a list of
    # the same info for the summary report.
    def wrap_up(self):
        wrap_up_info = []
        logging.info('Wrapping up...')
        sleep(1)
        if self._cla.deleteAllIds:
            self.delete_all_ids()

        if not self._return_to_startup_state():
            self.num_errors += 1
            logging.error('Failure reapplying startup config!')

        logging.info('Wrap-up complete, tests complete!')

        num_warnings = 'Number of Warnings: {}'.format(self.get_num_warnings())
        num_errors = '  Number of Errors: {}'.format(self.get_num_errors())

        if self.get_num_errors() > 0:
            overall_sts = '    Overall Status: FAILURE'
        elif self.get_num_warnings() > 0:
            overall_sts = '    Overall Status: SUCCESS (WITH WARNINGS)'
        else:
            overall_sts = '    Overall Status: SUCCESS'

        logging.info(num_warnings)
        logging.info(num_errors)
        logging.info(overall_sts)

        wrap_up_info.append('\t'+num_warnings)
        wrap_up_info.append('\t'+num_errors)
        wrap_up_info.append('\t'+overall_sts)

        sleep(1)
        return wrap_up_info
    #
    # End of Tests.wrap_up()

def execute(args, startup_state_applied):
    success = True
    test_configuration = 0              # Current test configuration counter
    num_test_configurations = (         # Total number of test configurations
        len(args.triggerReference) * len(args.startMode))

    start_banner = ' NEW DAT FILE TEST '.center(80,'=')
    end_banner = ' DAT FILE TEST COMPLETE '.center(80,'=')
    logging.info(start_banner)

    # NOTE: Any exceptions thrown during the test are not being caught here
    #       but rather in main as exceptions are fatal and we want to
    #       distinguish test failures from fatal errors.
    tests = Tests(args, startup_state_applied)

    if args.deleteAllIds:
        success = tests.delete_all_ids()

    success = tests.load_dat()

    if success:
        success = tests.assemble_dat()

    if success:
        tests.show_setup()

        dat_test_summary = []
        test_summary = ''
        for tr in args.triggerReference:
            # Create TriggerReference object from string
            trigger_reference = TriggerReference.create(tr)

            for sm in args.startMode:
                test_configuration += 1

                # Create StartMode object from string
                start_mode = StartMode.create(sm)

                # Begin assembling the test summary string and, formatted
                # slightly differently, add info to the log.
                test_cfg = 'Test Configuration {} of {}'.format(
                    test_configuration, num_test_configurations)

                tr_sm = '{},{}'.format(
                    trigger_reference.name(), start_mode.name())
                test_summary = '\t{}: {:>{w1}}: '.format(
                    test_cfg, tr_sm,
                     w1=MAX_WIDTH_TRIGGER_REFERENCE+MAX_WIDTH_START_MODE+1)

                logging.info(' {} '.format(test_cfg).center(80,'*'))
                logging.info('    triggerReference: {}'.format(
                    trigger_reference.name()))
                logging.info('           startMode: {}\n'.format(
                    start_mode.name()))

                # Initialize the CSF configurations with the current startMode
                # and triggerReference that will be used to execute the DAT on
                # the CSS for this set of mode tests.
                tests.initialize_test_configs(start_mode, trigger_reference)

                # For this combination of triggerReference/startMode, iterate
                # through each of the defined modes which may include one or
                # more of preview, abort, cancel, noAction.
                modes = ''        # holds comma separated modes
                results = ''      # holds comma separated PASS/FAIL for modes
                for mode in args.modesToTest:
                    if mode == MODE_PREVIEW:
                        success = tests.execute_preview_test(
                            trigger_reference)
                    else:
                        success = tests.execute_dat_test(
                            mode, start_mode, trigger_reference)
#                    success = True

                    # Append test mode and pass/fail to test summary string
                    pass_fail = 'PASS'
                    if not success: pass_fail = 'FAIL'

                    modes = modes + mode + ','
                    results = results + pass_fail + ','

                test_summary =''.join([test_summary, '[{}]=[{}]'.format(
                    modes.rstrip(','), results.rstrip(','))])

                # Add test summary to list
                dat_test_summary.append(test_summary)

    wrap_up_info = tests.wrap_up()

    logging.info(end_banner+'\n')

    # Get the flag that tracks whether the CSS startup state(initial
    # configuration) has been applied. If it has, and we're looping through DAT
    # tests, then we this flag will cause the next instantiation of Tests to
    # bypass submitting the CSS's initial configuration back to the CSS to
    # return it to it's initial startup state.
    startup_state_applied = tests.get_startup_state_applied()

    pass_fail = 'PASS'
    if tests.get_num_errors() > 0:
        success = False
        pass_fail = "FAIL"

    # Assemble this DAT file's test results
    # Overall DAT test status and filename
    dat_test_report = ['{}: {}'.format(pass_fail, args.datFile)]

    # Warnings/Errors counts
    dat_test_report.extend(wrap_up_info)

    # Individual <triggerReference>/<startMode> combo with modes results
    dat_test_report.extend(dat_test_summary)
    dat_test_report.append('')

    return (success, dat_test_report, startup_state_applied)
#
# End of execute()

def log_summary_report(startDate, endDate, aggregate_dat_report):
    totalRunTime = endDate - startDate

    summary_report = []
    summary_report.append('{}'.format(80*'='))
    summary_report.append('={}='.format('TEST SUMMARY'.center(78, ' ')))
    summary_report.append('{}'.format(80*'='))
    summary_report.append('Started @: {}'.format(
        str(startDate).split('.')[0]))
    summary_report.append('  Ended @: {}'.format(
        str(endDate).split('.')[0]))
    summary_report.append(' Duration: {}'.format(
        str(totalRunTime).split('.')[0]))
    summary_report.append('')
    summary_report.append('Overall test file status:')
    summary_report.extend(aggregate_dat_report)

    logging.info('Summary Report\n' + ''.join(
        '{}\n'.format(x) for x in summary_report))

#
# End of log_summary_report()


### Main program runtime ###

def main():
    global EXIT_CODE_SUCCESS
    global EXIT_CODE_FAILURE

    exit_code = EXIT_CODE_SUCCESS

    # Flag used when calling execute() which will instantiate the Test class.
    # During init(), the constructor will examine the value of this flag:
    #  If False - a configuration will be submitted to the CSS containing the
    #             initial configuration submitted from the Mgmt controller to
    #             the Cam controller as though the CSS were just started.
    #  If True - The above action is bypassed.
    # This is done to prevent redundent submits of the initial configuration
    # at the end of one DAT test and then again at the beginning of the next
    # DAT test.
    startup_state_applied = False

    # Configure a basic logger...
    logging.Formatter.converter = gmtime
    logging.basicConfig(
        level=logging.INFO,
        format='[%(asctime)s.%(msecs)03dZ] (%(levelname)8s) %(message)s',
        datefmt='%Y-%m-%dT%H:%M:%S')

    # Invoke the command line argparse. If the returned arguements contain a
    # non-empty 'datFile' value then we're running from the command line and
    # the contents of cmd_line_args are for this single datFile. Otherwise,
    # we're running from a testFile that contains JSON test definitions.
    # NOTE: --datFile and --testFile are mutually exclusive but required so we
    #       will only have one or the other.
    cla = cmdLineParser.parse_args()

    if cla.testFile:
        # Invoke the JsonArgFileProcessor to read and process the test file and
        # produce a command line argument list array where each element in the
        # array is a list of command line arguments for a test datFile.
        processor = JsonArgFileProcessor(
            cla.dluName,    # For building a complete set of args
            cla.testFile,   # The JSON Test definition file to process
            False)
        (success, list_of_tests) = processor.process_json_arg_file()

        if not success:
            return EXIT_CODE_FAILURE     # Error while processing JSON file
        elif len(list_of_tests) == 0:
            logging.error('No datFiles to run in \"{}\"...'
                '\'datsToRun\' is either empty or all tests are being skipped!'.format(
                    cla.testFile))
            return EXIT_CODE_SUCCESS
        else:
            # We now have an array of N lists where each list defines the
            # parameters for a DAT file test in command line argument form.
            # Run each list of arguments through the cmdLineParser to validate
            # each tests arguments. We do this here so that we reduce the chance
            # of a syntax/config error when we go to actually execute.
            for t in list_of_tests:
                cmdLineParser.parse_args(t)

    try:
        aggregate_dat_report = []       # Summary results for all datFiles

        startDate = datetime.utcnow()   # Save starting date/time

        if cla.datFile:       # Single dat file, cmdline arguments
            (success, dat_test_report) = execute(cla, startup_state_applied)
            aggregate_dat_report.extend(dat_test_report)

        else:                           # JSON Test file, N dats, args from list
            for t in list_of_tests:     # Parse test args and run
                args = cmdLineParser.parse_args(t)
                (success,
                 dat_test_report,
                 startup_state_applied) = execute(args, startup_state_applied)

                aggregate_dat_report.extend(dat_test_report)

        endDate = datetime.utcnow()     # Save ending date/time

        log_summary_report(startDate, endDate, aggregate_dat_report)

    except TestsInitException as tie:
        logging.error(traceback.format_exc())
        exit_code = EXIT_CODE_FAILURE
    except:
        logging.error('{}{}{}'.format(
            sys.exc_info()[0],
            sys.exc_info()[1],
            traceback.format_tb(sys.exc_info()[2])))
        exit_code = EXIT_CODE_FAILURE

    logging.shutdown()

    return exit_code
#
# End of Tests.main()

if __name__ == '__main__':
    sys.exit(main())


# Stuff to keep around for now...
# self.healthAT = AttributeTable()
# self.healthAT = self.healthAT.requalify(self._mgmt_ctrl_name)
# self.healthAT.insert('.csf:health')
# currHealthAT = self.vcc.get(self.healthAT)
# level  = currHealthAT.getString(self._mgmt_ctrl_name + '.csf:health:level')
# reason = currHealthAT.getString(self._mgmt_ctrl_name + '.csf:health:reason')
# if( level == 'bad' or level == 'ill' ):
#     self._num_errors += 1
#     success = False
#     print("HEALTH went %s while running DAT in preview for %0.3fsec, aborting test!\n"
#           "  Reason: %s"%(level,timeToWait,reason))

