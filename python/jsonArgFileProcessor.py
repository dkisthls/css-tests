import json

class JsonArgFileProcessor:
    def __init__(self, dlu, filename, debug):
        self._dlu_name = dlu
        self._test_definitions_file = filename
        self._debug = debug
        self._list_of_tests = []

    def process_json_arg_file(self):
        success = True

        try:
            with open(self._test_definitions_file, 'r') as f:
                test_definitions = json.load(f)

            # JSON files don't support comments so we have a "comment" paramater
            # to allow for user comments. It's not to be processed so the first
            # thing we're going to do is remove them

            # Get the 'defaults' dictionary and strip out all comments...
            d = test_definitions['defaults']
            default_args = {key: d[key] for key in d if key != 'comment'}
            if 'skip' in default_args:
                raise SyntaxError(
                    '\'skip\' cannot be used in the \'defaults\' section!')

            # Get the 'datsToRun' list which is a list of dictionaries and
            # strip out all comments from each dictionary...
            obj = test_definitions['datsToRun']
            dats_to_run = []
            for o in obj:
                dats_to_run.append(
                    {key: o[key] for key in o if key != 'comment'})

            if self._debug:
                print('TEST FILE: {}'.format(self._test_definitions_file))
                print('TEST FILE CONTENTS (sans comments):')
                print('defaults: {}'.format(json.dumps(default_args, indent=2)))
                print('datsToRun: {}'.format(json.dumps(dats_to_run, indent=2)))
                print('END OF TEST FILE CONTENTS\n')

            self._list_of_tests = []

            error = False
            for dat_to_run in dats_to_run:
                if self._debug:
                    print('{}\ndat_to_run: {}'.format(
                        20*'-', json.dumps(dat_to_run, indent=2)))

                # If the test args contain 'skip:true' then skip any processing.
                # Otherwise, verify that the dat_to_run defines a 'datFile'
                # name/value pair.
                if 'skip' in dat_to_run and dat_to_run['skip'] is True:
                    continue;
                elif not 'datFile' in dat_to_run:
                    raise SyntaxError(
                        'In {} - \'datsToRun\' entry is missing \'datFile\'!'.format(
                            self._test_definitions_file))
                elif(
                    not (isinstance(dat_to_run['datFile'], str) or
                         isinstance(dat_to_run['datFile'], unicode))
                    or dat_to_run['datFile'] == ""):
                    raise SyntaxError(
                        'In {} - \'datFile\' must be a non-empty string!'.format(
                            self._test_definitions_file))

                # Merge the default parameters with the test definition. The act
                # of merging will result in test parameters overwriting or
                # superceding any same named defaults.
                temp_args = default_args
                temp_args.update(dat_to_run)

                # For clarity in command-line argument generation, move the
                # 'datFile' to the front of the arguments list.'
                test_args = {}
                test_args['datFile'] = temp_args.pop('datFile')
                test_args.update(temp_args)

                if self._debug:
                    # The list of name/value(s) pairs for this test from the
                    # 'testFile'...
                    print('      MERGED ARGS: {}\n'.format(test_args))

                # Now that we have the arguments merged and semi-ordered the
                # name/value pairs can be converted to the correct form for use
                # as command-line arguemnts for abortCancelRunDAT.py.
                # Reset for a new set of formatted arguments.
                # The DLU name is always the first arg
                cmd_line_args = [self._dlu_name]

                for param in test_args:
                    value = test_args[param]

                    if (value is True or value is False):    # Boolean?
                        # 'skip' is not valid cmd_line_arg, Used to skip test
                        if param == 'skip':
                            if value is True:
                                skip_test = True
                                break;
                        elif value is True:    # Only include if 'true'...
                            cmd_line_args.append(str('--'+param))

                    elif isinstance(value, list):
                        # One cmd line arg with 1 or more values
                        cmd_line_args.append(str('--'+param))
                        for v in value:
                            cmd_line_args.append(str(v))   # Coerce to string
                    else:
                        # Standard cmd line arg/value pair
                        cmd_line_args.append(str('--'+param))
                        cmd_line_args.append(str(value))   # Coerce to string


                self._list_of_tests.append(cmd_line_args)
                if self._debug:
                    print('COMMAND LINE ARGS: {}\n'.format(cmd_line_args))

        except IOError as ioe:
            print('Failure opening test definitions file - {}'.format(ioe))
            success = False
        except (ValueError, SyntaxError) as err:
            print('Value/Syntax Error in {}'.format(self._test_definitions_file))
            print(err)
            success = False

        return(success, self._list_of_tests)
